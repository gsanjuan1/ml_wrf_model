#%% Carga de librerías.
import pandas as pd
from sklearn import preprocessing 
from sklearn.cluster import KMeans
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats
import numpy as np
#import researchpy as rp
from seaborn import load_dataset
import shutil
from os import remove
from itertools import product
import itertools
import pandas
import pickle

def kmeansApli(df, df2, numcluster):
    print('cluster:', numcluster)
    #%% Se normalizan los datos con MinMax()
    min_max_scaler = preprocessing.MinMaxScaler() 
    df_escalado = min_max_scaler.transform(df)
    df_escalado = pd.DataFrame(df_escalado) # Hay que convertir a DF el resultado.
    #%% Curva elbow para determinar valor óptimo de k.
    if False:
        nc = range(1, 200) # El número de iteraciones que queremos hacer.
        kmeans = [KMeans(n_clusters=i) for i in nc]
        score = [kmeans[i].fit(df_escalado).score(df_escalado) for i in range(len(kmeans))]
        score
        plt.xlabel('Número de clústeres (k)')
        plt.ylabel('Suma de los errores cuadráticos')
        plt.plot(nc,score)
        plt.show()
    #%% Aplicación de k-means con k = 5.

    kmeans = KMeans(n_clusters=numcluster).fit(df_escalado)
    centroids = kmeans.cluster_centers_
    #print(centroids)
    #%% Etiquetamos nuestro dataframe.
    #df.insert(0, 'myid', myid)
    labels = kmeans.predict(df_escalado)

    df['label'] = labels
    df2['label'] = labels
    #print(df)
    
    return df, df2, centroids

def kmeansAplival(df, df2, df_val,df_val2, numcluster):
    print('cluster:', numcluster)
    #%% Se normalizan los datos con MinMax()
    min_max_scaler = preprocessing.MinMaxScaler() 
    model=min_max_scaler.fit(df)
    df_escalado = model.transform(df)
    df_escalado = pd.DataFrame(df_escalado) # Hay que convertir a DF el resultado.
    #%% Curva elbow para determinar valor óptimo de k.

    kmeans = KMeans(n_clusters=numcluster).fit(df_escalado)
    centroids = kmeans.cluster_centers_
    #print(centroids)
    #%% Etiquetamos nuestro dataframe.
    #df.insert(0, 'myid', myid)
    labels = kmeans.predict(df_escalado)

    df2['label'] = labels
    #print(df)
    distance_max = calculodedistances(centroids, labels, df_escalado, numcluster)

    if len(df_val)>0:
        df_esc_val= model.transform(df_val)
        df_esc_val= pd.DataFrame(df_esc_val)
        #print()
        
        labels = kmeans.predict(df_esc_val)

        labels2 = modificacionlabel(centroids, labels, df_esc_val, numcluster,distance_max )
        df_val2['label'] = labels2
    return df2, centroids, df_val2

def preprocesingEscalado(df):

    #%% Se normalizan los datos con MinMax()
    min_max_scaler = preprocessing.MinMaxScaler() 
    model=min_max_scaler.fit(df)
    df_escalado = model.transform(df)
    df_escalado = pd.DataFrame(df_escalado) # Hay que convertir a DF el resultado.
    #pickle.dump(model, open(MODEL_FOLDER +f"{TIPO}_{i}_{hour}_maxmin_esc.pkl", "wb"))
    #%% Curva elbow para determinar valor óptimo de k.
    return model

def kmeansApli3(df, df2, numcluster, valor_init, model):
    print('cluster:', numcluster)
    #%% Se normalizan los datos con MinMax()

    df_escalado = model.transform(df)
    df_escalado = pd.DataFrame(df_escalado) # Hay que convertir a DF el resultado.
    #%% Curva elbow para determinar valor óptimo de k.

    kmeans = KMeans(n_clusters=numcluster,n_init = valor_init, random_state=0).fit(df_escalado)
    centroids = kmeans.cluster_centers_
    #print(centroids)
    #%% Etiquetamos nuestro dataframe.
    #df.insert(0, 'myid', myid)
    labels = kmeans.predict(df_escalado)

    df2['label'] = labels
    #print(df)

    return df2, centroids, kmeans

def kmeansApli2(df, df2, numcluster, valor_init, model):
    print('cluster:', numcluster)
    #%% Se normalizan los datos con MinMax()
    min_max_scaler = preprocessing.MinMaxScaler() 
    model=min_max_scaler.fit(df)
    df_escalado = model.transform(df)
    df_escalado = pd.DataFrame(df_escalado) # Hay que convertir a DF el resultado.
    #%% Curva elbow para determinar valor óptimo de k.

    kmeans = KMeans(n_clusters=numcluster,n_init = valor_init).fit(df_escalado)
    centroids = kmeans.cluster_centers_
    #print(centroids)
    #%% Etiquetamos nuestro dataframe.
    #df.insert(0, 'myid', myid)
    labels = kmeans.predict(df_escalado)

    df2['label'] = labels
    #print(df)

    return df2, centroids, kmeans, model

def kmeansVals(df, df_val, df_val2, centroids, kmeans, model, labels_use, numcluster):
    #print(df)

    df_escalado = model.transform(df)
    df_escalado = pd.DataFrame(df_escalado)
    
    distance_max = calculodedistances2(centroids, labels_use, df_escalado, numcluster)

    if len(df_val)>0:
        df_esc_val= model.transform(df_val)
        df_esc_val= pd.DataFrame(df_esc_val)
        #print()
        
        labels = kmeans.predict(df_esc_val)

        labels2 = modificacionlabel2(centroids, labels, df_esc_val, numcluster,distance_max )
        df_val2['label'] = labels2
        df_val2 =  df_val2.query(f'label =={labels_use}')
        return df_val2


def kmeansVals2(df, df_val, df_val2, centroids, kmeans, model, labels_use, numcluster, numVariables):
    #print(df)

    df_escalado = model.transform(df)
    df_escalado = pd.DataFrame(df_escalado)
    
    distance_max = calculodedistances2(centroids, labels_use, df_escalado, numcluster)

    if len(df_val)>0:
        df_esc_val= model.transform(df_val)
        df_esc_val= pd.DataFrame(df_esc_val)
        #print()
        
        labels = kmeans.predict(df_esc_val)

        labels2, dist_euc = modificacionlabel3(centroids, labels, df_esc_val, numcluster,distance_max )
        #print(dist_euc)
        df_val2['label'] = labels2
        df_val2['dic_euc'] = dist_euc/numVariables
        #print(df_val2)
        df_val2 =  df_val2.query(f'label =={labels_use}')
        return df_val2


def calculodedistances(centroide, labels, df_escalado, numcluster):

    df_escalado['labels'] = labels
    distance = []
    for i in range(numcluster):
        distance.append(0)

    labls_use = df_escalado['labels'].unique()
    
    for labls in labls_use:
        df_escalado2 = df_escalado.query(f"labels == {labls}")
        df_escalado2 = df_escalado2.drop(['labels'], axis =1)
        #print(centroide[0])
        #print(pow(df_escalado2 - centroide[labls],2).sum(axis = 1))
        df_escalado2['distancia'] = pow(df_escalado2 - centroide[labls],2).sum(axis = 1)
        df_escalado2['distancia'].max()
        #print('Maximo', df_escalado2['distancia'].max())
        distance[labls] = df_escalado2['distancia'].max()
    return distance

def calculodedistances2(centroide, labels, df_escalado, numcluster):

    #print(centroide[0])
    #print(pow(df_escalado2 - centroide[labls],2).sum(axis = 1))
    df_escalado['distancia'] = pow(pow(df_escalado - centroide[labels],2).sum(axis = 1),0.5)
    #df_escalado['distancia'].max()
    #print('Maximo', df_escalado2['distancia'].max())
    distance = df_escalado['distancia'].mean()
    #print(df_escalado['distancia'])
    return distance

def modificacionlabel(centroide, labels, df_esc_val, numcluster,distance):
    #print(labels)
    #print(df_esc_val)
    #print(len(centroide))
    centroide= pd.DataFrame(centroide)
    #print(centroide)
    #print(df_esc_val)
    df_esc_val2 = df_esc_val
    df_esc_val['labels'] = labels
    
    df_esc_val2 = df_esc_val2.drop(['labels'], axis =1)
    #print(df_esc_val)

    
    for kk in df_esc_val.index:
        
        df_esc_val.loc[kk, 'dic_euc'] = pow(pow(df_esc_val2.loc[kk] - centroide.loc[df_esc_val.loc[kk, 'labels']],2).sum(),0.5)
    
    #print(df_esc_val)
    for kk in df_esc_val.index:
        if df_esc_val.loc[kk,'dic_euc'] >= distance[df_esc_val.loc[kk,'labels']]:
            #print('entro, ',df_esc_val.loc[kk,'dic_euc'],distance[df_esc_val.loc[kk,'labels']])
            labels[kk] = -1
    
    #print(labels)

    return labels

def modificacionlabel2(centroide, labels, df_esc_val, numcluster,distance):
    #print(labels)
    #print(df_esc_val)
    #print(len(centroide))
    centroide= pd.DataFrame(centroide)
    #print(centroide)
    #print(df_esc_val)
    df_esc_val2 = df_esc_val
    df_esc_val['labels'] = labels
    
    df_esc_val2 = df_esc_val2.drop(['labels'], axis =1)
    #print(df_esc_val)

    
    for kk in df_esc_val.index:
        
        df_esc_val.loc[kk, 'dic_euc'] = pow(pow(df_esc_val2.loc[kk] - centroide.loc[df_esc_val.loc[kk, 'labels']],2).sum(),0.5)
    
    #print(df_esc_val)
    for kk in df_esc_val.index:
        if df_esc_val.loc[kk,'dic_euc'] >= distance:
            #print('entro, ',df_esc_val.loc[kk,'dic_euc'],distance[df_esc_val.loc[kk,'labels']])
            labels[kk] = -1
    
    #print(labels)

    return labels


def modificacionlabel3(centroide, labels, df_esc_val, numcluster,distance):
    #print(labels)
    #print(df_esc_val)
    #print(len(centroide))
    centroide= pd.DataFrame(centroide)
    #print(centroide)
    #print(df_esc_val)
    df_esc_val2 = df_esc_val
    df_esc_val['labels'] = labels
    
    df_esc_val2 = df_esc_val2.drop(['labels'], axis =1)
    #print(df_esc_val)

    
    for kk in df_esc_val.index:
        
        df_esc_val.loc[kk, 'dic_euc'] = pow(pow(df_esc_val2.loc[kk] - centroide.loc[df_esc_val.loc[kk, 'labels']],2).sum(),0.5)
    
    #print(df_esc_val)
    #print(distance)
    for kk in df_esc_val.index:
        if df_esc_val.loc[kk,'dic_euc'] >= distance:
            #print('entro, ',df_esc_val.loc[kk,'dic_euc'],distance[df_esc_val.loc[kk,'labels']])
            labels[kk] = -1
    
    #print(labels)

    return labels, df_esc_val['dic_euc'].to_numpy()


                    
def kmeansPredict(df, df2, kmeans, model):

    df_esc_val= model.transform(df)
    df_esc_val= pd.DataFrame(df_esc_val)
    labels = kmeans.predict(df_esc_val)
    df2['label'] = labels

    return df2


def guardarkmeans(centroide, labels, df_escalado,model, numcluster, MODEL_FOLDER,name_save):

    #print(centroide[0])
    #print(df_escalado)
    df_escalado = model.transform(df_escalado)
    df_escalado = pd.DataFrame(df_escalado)
    #print(pow(df_escalado2 - centroide[labls],2).sum(axis = 1))
    df_escalado['distancia'] = pow(pow(df_escalado - centroide[labels],2).sum(axis = 1),0.5)
    #df_escalado['distancia'].max()
    #print('Maximo', df_escalado2['distancia'].max())
    distance = df_escalado['distancia'].mean()
    #print(distance)
    strn= ''
    for cen in centroide[labels]:
        strn= strn + ';'+ str(cen) 
    f = open(MODEL_FOLDER +'centroide_kmeans.txt','a')
    f.write(name_save  + ',' + str(distance) + ',' )
    f.write(strn)
    f.write('\n')
    f.close()
    #return distance


