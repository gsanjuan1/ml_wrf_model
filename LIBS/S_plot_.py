import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
import scipy
import math

def plotScattered(y1, y2, color,xname,yname,title, xmax, text,size_point):
    X = np.array(y2).reshape(-1, 1)
    y = y1
    reg = LinearRegression(fit_intercept=False).fit(X, y)
    R2 = round(reg.score(X, y),2)
    plt.scatter(y1, y2, s=size_point,alpha=.2, label=R2,color=color)
    #plt.annotate(len(y1), xy=(0.90, 0.95), xycoords='axes fraction', fontsize=5)
    plt.xlabel(xname)
    plt.ylabel(yname)
    plt.title(title)
    plt.ylim(0, xmax)
    plt.xlim(0,xmax)
    plt.plot(plt.xlim(), plt.ylim(), linewidth=0.2, color="green")
    #plt.legend(loc='best')
    plt.grid(True)
    #print(R2)


def PlotGauss(mean,std,xmin,xmax,nx ):
    x = np.linspace(xmin,xmax, nx)
    y =scipy.stats.norm.pdf(x,mean,std)
    plt.plot(x,y, color='red', label="xxx")  
    
def PlotGaus(data,xmin,xmax,nx):
    std=data.std()
    mean=data.mean()
    PlotGauss(mean,std,xmin,xmax,nx )
    

