import math
import random
import pandas as pd
import LIBS.S_plot_
import matplotlib.pyplot as plt
import numpy as np
import os
import seaborn as sns
from sklearn.metrics import mean_squared_error


def resultRF(error_ini, error_train, dev_ini, dev_train, des_st_ini, des_st_train, TIPO):
    mean_st = f"dev {round(dev_ini,2)} -> {round(dev_train,2)} ({100-round(100*dev_train/dev_ini,0)})"
    des_st2 = f"devst {round(des_st_ini,2)} -> {round(des_st_train,2)} ({100-round(100*des_st_train/des_st_ini,0)})"
    dev_st = f"mean {round(error_ini.mean(),2)} -> {round(error_train.mean(),2)}"
    #dimension = f"n.point {len(error_ini)}"
    print(TIPO, "Mean:",mean_st,", Std:",dev_st)

def SampleCharacteristics(X_train,X_test,df_train,df_test, folder, name_file = 'caracteristicas_eje_analisis.txt' ):
    datos = "TRAIN :" + "," + str(X_train.shape[0])+ ","  +"TEST:"+ ","  + str(X_test.shape[0])+ ","  + 'TRAIN_M15:'+ ","  + str(len(df_train['wind_event'][df_train['wind_event']==1]))+ "," +'TEST_M15:'+ "," +str(len(df_test['wind_event'][df_test['wind_event']==1]))
    f = open(folder + 'caracteristicas_eje_analisis.txt','a') 
    f.write(datos)
    f.write(f"\n" )
    f.close()

def AplotScatter(df_target, df_compare, y_pred, folder, name_file):
    LIBS.S_plot_.plotScattered(df_target, df_compare, 'blue', 'Modulus of wind speed in station (mph)', 'Modulus of wind speed in wrf/pred (mph)', '', 40, 40,6)
    LIBS.S_plot_.plotScattered(df_target, y_pred, 'red', 'Modulus of wind speed in station (mph)', 'Modulus of wind speed in wrf/pred (mph)', '', 40, 40, 6)
    plt.savefig(folder + name_file)
    plt.clf()  

def Only_AplotScatter(df_target, df_compare, color, folder, name_file,name_ejeY):
    LIBS.S_plot_.plotScattered(df_target, df_compare, color, 'Modulus of wind speed in station (mph)', name_ejeY, '', 40, 40,9)
    plt.savefig(folder + name_file)
    plt.clf()  

def CompErr(df_target, df_compare, y_pred):

    error_ini = np.subtract(df_compare, df_target)
    error_train = np.subtract(y_pred, df_target)
    dev_ini = np.sum(np.square(error_ini))/len(error_ini)
    dev_train = np.sum(np.square(error_train))/len(error_train)
    des_st_ini = np.square(np.sum(pow(error_ini - error_ini.mean(),2))/len(error_ini))
    des_st_train = np.square(np.sum(pow(error_train - error_train.mean(),2))/len(error_ini))
    
    return error_ini, error_train, dev_ini, dev_train, des_st_ini, des_st_train

def ResultSimulation(tipo, estacion_train, estacion, X_train, error_ini, error_train,des_ini, des_train , iteration, folder, name_file):
    datos = f'{tipo}_PRUEBA:'+ 'STATION_TRAIN'+ "," +str(estacion_train)+ "," +'STATION'+ "," + str(estacion)+ "," +  str(iteration) + "," + f"{tipo} :" + "," + str(X_train.shape[0])+ ","
    f = open(folder + name_file ,'a') 
    f.write(datos)
    datos ='mean_sts:' + "," +str(round(error_ini.mean(),2))+ "," +  'mean_wrf:'+ "," + str(round(error_train.mean(),2))+ "," 
    f.write(datos)
    datos ='dev_sts:' + "," + str(round(des_ini,2))+ "," +  'dev_train:' + "," + str(round(des_train,2))  + "," + "Mejora" + "," +  f"{100-round(100*des_train/des_ini,0)}"+ ","   
    f.write(datos)
    datos = 'M5_sts:'+ ","  + str(len(error_ini[error_ini > 5]))+ "," + str(round(len(error_ini[error_ini > 5])*100/X_train.shape[0],2)) + "," 
    f.write(datos)
    datos = 'M5_wrf:'+ ","  + str(len(error_train[error_train > 5]))+ ","  + str(round(len(error_train[error_train > 5])*100/X_train.shape[0],2)) + "," 
    f.write(datos)  
    datos = 'm5_sts:'+ ","  + str(len(error_ini[error_ini <-5]))+ "," + str(round(len(error_ini[error_ini <-5])*100/X_train.shape[0],2)) + "," 
    f.write(datos)
    datos = 'm5_wrf:'+ ","  + str(len(error_train[error_train <-5]))+ ","  + str(round(len(error_train[error_train < -5])*100/X_train.shape[0],2)) + "," 
    f.write(datos)
    datos = 'M10_sts:'+ ","  + str(len(error_ini[error_ini > 10]))+ ","  + str(round(len(error_ini[error_ini > 10])*100/X_train.shape[0],2)) + "," 
    f.write(datos)
    datos = 'M10_wrf:'+ ","  + str(len(error_train[error_train > 10]))+ ","  + str(round(len(error_train[error_train > 10])*100/X_train.shape[0],2)) + ","   
    f.write(datos)
    datos = 'm10_sts:'+ ","  + str(len(error_ini[error_ini <-10]))+ "," + str(round(len(error_ini[error_ini <-10])*100/X_train.shape[0],2)) + "," 
    f.write(datos)
    datos = 'm10_wrf:'+ ","  + str(len(error_train[error_train <-10]))+ ","  + str(round(len(error_train[error_train < -10])*100/X_train.shape[0],2)) + ","  
    f.write(datos)
    datos = 'M15_sts:'+ ","  + str(len(error_ini[error_ini > 15]))+ ","  + str(round(len(error_ini[error_ini > 15])*100/X_train.shape[0],2)) + "," 
    f.write(datos)
    datos = 'M15_wrf:'+ ","  + str(len(error_train[error_train > 15]))+ ","  + str(round(len(error_train[error_train > 15])*100/X_train.shape[0],2)) + ","   
    f.write(datos)
    datos = 'm15_sts:'+ ","  + str(len(error_ini[error_ini <-15]))+ "," + str(round(len(error_ini[error_ini <-15])*100/X_train.shape[0],2)) + ","  
    f.write(datos)
    datos = 'm15_wrf:'+ ","  + str(len(error_train[error_train <-15]))+ "," + str(round(len(error_train[error_train < -15])*100/X_train.shape[0],2)) + ","
    f.write(datos)
    f.write(f"\n" )
    f.close() 

def histOnly(error_ini, error_train, dev_ini, dev_train, des_st_ini, des_st_train, folder, name_file):
    
    mean_st = f"dev {round(dev_ini,2)} -> {round(dev_train,2)} ({100-round(100*dev_train/dev_ini,0)})"
    des_st2 = f"devst {round(des_st_ini,2)} -> {round(des_st_train,2)} ({100-round(100*des_st_train/des_st_ini,0)})"
    dev_st = f"mean {round(error_ini.mean(),2)} -> {round(error_train.mean(),2)}"
    #dimension = f"n.point {len(error_ini)}"
    print("Mean:",mean_st,", Std:",dev_st)

    xmin = -15
    xmax = 15
    mybins = 100
    plt.hist(error_ini, range=(xmin, xmax), bins=mybins, color='blue', histtype='step', density=True)
    plt.hist(error_train, range=(xmin, xmax), bins=mybins, color='red', histtype='step', density=True)
    plt.annotate(mean_st, xy=(0.03, 0.95), xycoords='axes fraction', fontsize=8)
    plt.annotate(dev_st, xy=(0.03, 0.90), xycoords='axes fraction', fontsize=8)
    plt.annotate(des_st2, xy=(0.03, 0.85), xycoords='axes fraction', fontsize=8)
    #plt.annotate(dimension, xy=(0.03, 0.80), xycoords='axes fraction', fontsize=8)
    plt.savefig(folder + name_file)
    plt.clf()

def histMulti(error_ini, error_train, dev_ini, dev_train, des_st_ini, des_st_train, dataset, PREDICT, COMPARE, TARGET, folder, name_file):    
    


    dataset['wind_error'] = error_ini
    dataset['wind_error_corr'] = error_train
    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2)
    mean_st = f"dev {round(dev_ini,2)} -> {round(dev_train,2)} ({100-round(100*dev_train/dev_ini,0)})"
    dev_st = f"mean {round(error_ini.mean(),2)} -> {round(error_train.mean(),2)}"
    des_st2 = f"devst {round(des_st_ini,2)} -> {round(des_st_train,2)} ({100-round(100*des_st_train/des_st_ini,0)})"
    dimension = f"n.point {len(error_ini)}"
    print("Mean:",mean_st,", Std:",dev_st)

    xmin = -15
    xmax = 15
    mybins = 100
    
    #df_rang1 = dataset.query(f'sts_w >=5 & sts_w <=10')
    #df_rang2 = dataset.query(f'{PREDICT} >=5 & {PREDICT} <=10')
    df_rang1 = dataset.query(f'sts_w > 10')
    df_rang2 = dataset.query(f'{PREDICT} >10')
    ax1.hist(error_ini, range=(xmin, xmax), bins=mybins, color='blue', histtype='step', density=True, label = 'Initial Error')
    ax1.hist(error_train, range=(xmin, xmax), bins=mybins, color='red', histtype='step', density=True, label = 'RF Error')
    ax1.annotate(mean_st, xy=(0.03, 0.95), xycoords='axes fraction', fontsize=5)
    ax1.annotate(dev_st, xy=(0.03, 0.90), xycoords='axes fraction', fontsize=5)
    ax1.annotate(des_st2, xy=(0.03, 0.85), xycoords='axes fraction', fontsize=5)
    ax1.annotate(dimension, xy=(0.03, 0.80), xycoords='axes fraction', fontsize=5)
    ax1.legend(fontsize=5)
    ax1.set_ylim([0, 0.4])
    ax2.hist(dataset[dataset['sts_w'] <= 10]['wind_error'].values, range=(xmin, xmax), bins=mybins, color='royalblue', histtype='step', density=True, label = '(0-10) Error')
    ax2.hist(dataset[dataset['sts_w'] <= 10]['wind_error_corr'].values, range=(xmin, xmax), bins=mybins, color='deeppink', histtype='step', density=True, label = '(0-10) RF Error')
    df_p = dataset.query(f'sts_w <5 & wind_error < -10')
    volum = f"volum {round(len(df_p)*100/len(dataset),2)}"
    df_p = dataset.query(f'sts_w <10 & wind_error_corr < -10')
    volum2 = f"volum pred {round(len(df_p)*100/len(dataset),2)}"
    ax2.annotate(volum, xy=(0.03, 0.95), xycoords='axes fraction', fontsize=5)
    ax2.annotate(volum2, xy=(0.03, 0.90), xycoords='axes fraction', fontsize=5)
    ax2.legend(fontsize=5)
    ax2.set_ylim([0, 0.4])
    ax3.hist(df_rang1['wind_error'].values, range=(xmin, xmax), bins=mybins, color='deepskyblue', histtype='step', density=True, label = '(>10) Error')
    ax3.hist(df_rang1['wind_error_corr'].values, range=(xmin, xmax), bins=mybins, color='lightcoral', histtype='step', density=True, label = '(>10) RF Error')
    volum = f"volum {round(len(df_rang1[df_rang1['wind_error']<-10.0 ]['wind_error'])*100/len(dataset),2)}"
    volum2 = f"volum {round(len(df_rang2[df_rang2['wind_error_corr']<-10.0 ]['wind_error_corr'])*100/len(dataset),2)}"
    ax3.annotate(volum, xy=(0.03, 0.95), xycoords='axes fraction', fontsize=5)
    ax3.annotate(volum2, xy=(0.03, 0.90), xycoords='axes fraction', fontsize=5)
    ax3.legend(fontsize=5)
    ax3.set_ylim([0, 0.4])
    ax4.hist(dataset[dataset['sts_w'] > 15]['wind_error'].values, range=(xmin, xmax), bins=mybins, color='dodgerblue', histtype='step', density=True, label = '(>15) Error')
    ax4.hist(dataset[dataset['sts_w'] > 15 ]['wind_error_corr'].values, range=(xmin, xmax), bins=mybins, color='magenta', histtype='step', density=True, label = '(>15) RF Error')
    df_p = dataset.query(f'sts_w >15 & wind_error < -15')
    volum = f"volum {round(len(df_p)*100/len(dataset),2)}"
    df_p = dataset.query(f'sts_w >15 & wind_error_corr < -15')
    volum2 = f"volum pred {round(len(df_p)*100/len(dataset),2)}"
    ax4.annotate(volum, xy=(0.03, 0.95), xycoords='axes fraction', fontsize=5)
    ax4.annotate(volum2, xy=(0.03, 0.90), xycoords='axes fraction', fontsize=5)
    ax4.legend(fontsize=5)
    ax4.set_ylim([0, 0.4])
    plt.savefig(folder +name_file)
    plt.clf()


def histMulti_big_range(error_ini, error_train, dev_ini, dev_train, des_st_ini, des_st_train, dataset, PREDICT, COMPARE, TARGET, folder, name_file):    
    


    dataset['wind_error'] = error_ini
    dataset['wind_error_corr'] = error_train
    dataset['wind_error_diff'] = abs(error_ini) - abs(error_train)


    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2)
    mean_st = f"dev {round(dev_ini,2)} -> {round(dev_train,2)} ({100-round(100*dev_train/dev_ini,0)})"
    dev_st = f"mean {round(error_ini.mean(),2)} -> {round(error_train.mean(),2)}"
    des_st2 = f"devst {round(des_st_ini,2)} -> {round(des_st_train,2)} ({100-round(100*des_st_train/des_st_ini,0)})"
    dimension = f"n.point {len(error_ini)}"
    Mejora = dataset.query("wind_error_diff>2")
    Peor =   dataset.query("wind_error_diff< -2")
    igual =  dataset.query("wind_error_diff>= -2 & wind_error_diff<=2")
    Best = f"Best {round(len(Mejora)*100/len(dataset),0)}"
    Bad = f"Bad {round(len(Peor)*100/len(dataset),0)}"
    Igual = f"Same {round(len(igual)*100/len(dataset),0)}"

    print("Mean:",mean_st,", Std:",dev_st)

    xmin = -15
    xmax = 15
    mybins = 100
    
    #df_rang1 = dataset.query(f'sts_w >=5 & sts_w <=10')
    #df_rang2 = dataset.query(f'{PREDICT} >=5 & {PREDICT} <=10')
    df_rang1 = dataset.query(f'sts_w > 8')
    df_rang2 = dataset.query(f'{PREDICT} >8')
    ax1.hist(error_ini, range=(xmin, xmax), bins=mybins, color='blue', histtype='step', density=True, label = 'Initial Error')
    ax1.hist(error_train, range=(xmin, xmax), bins=mybins, color='red', histtype='step', density=True, label = 'RF Error')
    ax1.annotate(mean_st, xy=(0.03, 0.95), xycoords='axes fraction', fontsize=5)
    ax1.annotate(dev_st, xy=(0.03, 0.90), xycoords='axes fraction', fontsize=5)
    ax1.annotate(des_st2, xy=(0.03, 0.85), xycoords='axes fraction', fontsize=5)
    ax1.annotate(dimension, xy=(0.03, 0.80), xycoords='axes fraction', fontsize=5)
    ax1.annotate(Best, xy=(0.03, 0.75), xycoords='axes fraction', fontsize=5)
    ax1.annotate(Bad, xy=(0.03, 0.70), xycoords='axes fraction', fontsize=5)
    ax1.annotate(Igual, xy=(0.03, 0.65), xycoords='axes fraction', fontsize=5)
    ax1.legend(fontsize=5)
    ax1.set_ylim([0, 0.4])
    ax2.hist(dataset[dataset['sts_w'] <= 8]['wind_error'].values, range=(xmin, xmax), bins=mybins, color='royalblue', histtype='step', density=True, label = '(0-8) Error')
    ax2.hist(dataset[dataset['sts_w'] <= 8]['wind_error_corr'].values, range=(xmin, xmax), bins=mybins, color='deeppink', histtype='step', density=True, label = '(0-8) RF Error')
    
    
    
    #des_st_ini = np.square(np.sum(pow(error_ini - error_ini.mean(),2))/len(error_ini))
    #des_st_train = np.square(np.sum(pow(error_train - error_train.mean(),2))/len(error_ini))
    std_ini2 = dataset[dataset['sts_w'] <= 8]['wind_error'].to_numpy()
    std_train2 = dataset[dataset['sts_w'] <= 8]['wind_error_corr'].to_numpy()
    mean_ini = dataset[dataset['sts_w'] <= 8]['wind_error'].mean()
    mean_train = dataset[dataset['sts_w'] <= 8]['wind_error_corr'].mean()
    dataset_count = dataset.query("sts_w<=8 ")
    Mejora = dataset.query("sts_w<=8 & wind_error_diff>2")
    Peor =   dataset.query("sts_w<=8 & wind_error_diff< -2")
    igual =  dataset.query("sts_w<=8 & wind_error_diff>= -2 & wind_error_diff<=2")
    if len(dataset_count) !=0:
        Best = f"Best {round(len(Mejora)*100/len(dataset_count),0)}"
        Bad = f"Bad {round(len(Peor)*100/len(dataset_count),0)}"
        Igual = f"Same {round(len(igual)*100/len(dataset_count),0)}"
    else:
        Best = f"Best 0"
        Bad = f"Bad 0"
        Igual = f"Same 0"

    std_ini = np.sum(np.square(std_ini2))/len(std_ini2)
    std_train = np.sum(np.square(std_train2))/len(std_train2)
    #std_ini = np.square(np.sum( pow(std_ini2 - mean_ini,2)))/len(std_ini2)
    #std_train = np.square(np.sum( pow(std_train2 - mean_train,2)))/len(std_train2)
    mean_st = f"dev ({100-round(100*std_train/std_ini,0)}) {round(std_ini,2)} -> {round(std_train,2)}"
    dev_st = f"mean {round(mean_ini,2)} -> {round(mean_train,2)}"  
    dimension = f"n.point {len(std_train2)}" 
    ax2.annotate(mean_st, xy=(0.03, 0.95), xycoords='axes fraction', fontsize=5)
    ax2.annotate(dev_st, xy=(0.03, 0.90), xycoords='axes fraction', fontsize=5)
    ax2.annotate(dimension, xy=(0.03, 0.85), xycoords='axes fraction', fontsize=5)
    ax2.annotate(Best, xy=(0.03, 0.80), xycoords='axes fraction', fontsize=5)
    ax2.annotate(Bad, xy=(0.03, 0.75), xycoords='axes fraction', fontsize=5)
    ax2.annotate(Igual, xy=(0.03, 0.70), xycoords='axes fraction', fontsize=5)
    ax2.legend(fontsize=5)
    ax2.set_ylim([0, 0.4])
    
    ax3.hist(df_rang1['wind_error'].values, range=(xmin, xmax), bins=mybins, color='deepskyblue', histtype='step', density=True, label = '(>8) Error')
    ax3.hist(df_rang1['wind_error_corr'].values, range=(xmin, xmax), bins=mybins, color='lightcoral', histtype='step', density=True, label = '(>8) RF Error')
    std_ini2 = dataset[dataset['sts_w'] > 8]['wind_error'].to_numpy()
    std_train2 = dataset[dataset['sts_w'] > 8]['wind_error_corr'].to_numpy()
    mean_ini = dataset[dataset['sts_w'] > 8]['wind_error'].mean()
    mean_train = dataset[dataset['sts_w'] > 8]['wind_error_corr'].mean()
    
    dataset_count = dataset.query("sts_w>8 ")
    Mejora = dataset.query("sts_w>8 & wind_error_diff>2")
    Peor =   dataset.query("sts_w>8 & wind_error_diff< -2")
    igual =  dataset.query("sts_w>8 & wind_error_diff>= -2 & wind_error_diff<=2")
    if len(dataset_count) !=0:
        Best = f"Best {round(len(Mejora)*100/len(dataset_count),0)}"
        Bad = f"Bad {round(len(Peor)*100/len(dataset_count),0)}"
        Igual = f"Same {round(len(igual)*100/len(dataset_count),0)}"
    else:
        Best = f"Best 0"
        Bad = f"Bad 0"
        Igual = f"Same 0"
    
    std_ini = np.sum(np.square(std_ini2))/len(std_ini2)
    std_train = np.sum(np.square(std_train2))/len(std_train2)
    #std_ini = np.square(np.sum( pow(std_ini2 - mean_ini,2)))/len(std_ini2)
    #std_train = np.square(np.sum( pow(std_train2 - mean_train,2)))/len(std_train2)
    mean_st = f"dev ({100-round(100*std_train/std_ini,0)}) {round(std_ini,2)} -> {round(std_train,2)}"
    dev_st = f"mean {round(mean_ini,2)} -> {round(mean_train,2)}"
    dimension = f"n.point {len(std_train2)}"    
    ax3.annotate(mean_st, xy=(0.03, 0.95), xycoords='axes fraction', fontsize=5)
    ax3.annotate(dev_st, xy=(0.03, 0.90), xycoords='axes fraction', fontsize=5)
    ax3.annotate(dimension, xy=(0.03, 0.85), xycoords='axes fraction', fontsize=5)
    ax3.annotate(Best, xy=(0.03, 0.80), xycoords='axes fraction', fontsize=5)
    ax3.annotate(Bad, xy=(0.03, 0.75), xycoords='axes fraction', fontsize=5)
    ax3.annotate(Igual, xy=(0.03, 0.70), xycoords='axes fraction', fontsize=5)
    ax3.legend(fontsize=5)
    ax3.set_ylim([0, 0.4])
    
    ax4.hist(dataset[dataset['sts_w'] > 10]['wind_error'].values, range=(xmin, xmax), bins=mybins, color='dodgerblue', histtype='step', density=True, label = '(>10) Error')
    ax4.hist(dataset[dataset['sts_w'] > 10 ]['wind_error_corr'].values, range=(xmin, xmax), bins=mybins, color='magenta', histtype='step', density=True, label = '(>10) RF Error')
    
    std_ini2 = dataset[dataset['sts_w'] > 10]['wind_error'].to_numpy()
    std_train2 = dataset[dataset['sts_w'] > 10]['wind_error_corr'].to_numpy()
    mean_ini = dataset[dataset['sts_w'] > 10]['wind_error'].mean()
    mean_train = dataset[dataset['sts_w'] > 10]['wind_error_corr'].mean()
    
    dataset_count = dataset.query("sts_w>10 ")
    Mejora = dataset.query("sts_w>10 & wind_error_diff>2")
    Peor =   dataset.query("sts_w>10 & wind_error_diff< -2")
    igual =  dataset.query("sts_w>10 & wind_error_diff>= -2 & wind_error_diff<=2")
    if len(dataset_count) !=0:
        Best = f"Best {round(len(Mejora)*100/len(dataset_count),0)}"
        Bad = f"Bad {round(len(Peor)*100/len(dataset_count),0)}"
        Igual = f"Same {round(len(igual)*100/len(dataset_count),0)}"
    else:
        Best = f"Best 0"
        Bad = f"Bad 0"
        Igual = f"Same 0"
    #std_ini = np.square(np.sum( pow(std_ini2 - mean_ini,2)))/len(std_ini2)
    #std_train = np.square(np.sum( pow(std_train2 - mean_train,2)))/len(std_train2)
    std_ini = np.sum(np.square(std_ini2))/len(std_ini2)
    std_train = np.sum(np.square(std_train2))/len(std_train2)
    mean_st = f"dev ({100-round(100*std_train/std_ini,0)}) {round(std_ini,2)} -> {round(std_train,2)} "
    dev_st = f"mean {round(mean_ini,2)} -> {round(mean_train,2)}"   
    dimension = f"n.point {len(std_train2)}"
    ax4.annotate(mean_st, xy=(0.03, 0.95), xycoords='axes fraction', fontsize=5)
    ax4.annotate(dev_st, xy=(0.03, 0.90), xycoords='axes fraction', fontsize=5)
    ax4.annotate(dimension, xy=(0.03, 0.85), xycoords='axes fraction', fontsize=5)
    ax4.annotate(Best, xy=(0.03, 0.75), xycoords='axes fraction', fontsize=5)
    ax4.annotate(Bad, xy=(0.03, 0.70), xycoords='axes fraction', fontsize=5)
    ax4.annotate(Igual, xy=(0.03, 0.65), xycoords='axes fraction', fontsize=5)
    ax4.legend(fontsize=5)
    ax4.set_ylim([0, 0.4])
    plt.savefig(folder +name_file)
    plt.clf()

def savefeature(station, varaibles, feature,folder, name_file):

    count = 0
    variables2 =''
    for var in  varaibles:
        count =count +1
        if count != len(varaibles):
            variables2 = variables2+ str(var) + ','
        else:
            variables2 = variables2 + str(var)

    datos = f'STATION'+ ";" + str(station)+ ";" + str(feature) + ";" +  str(variables2)
    f = open(folder + name_file ,'a') 
    f.write(datos)
    f.write(f"\n" )
    f.close() 


def ResultCombinacion(tipo, feature, resticciones, estacion_train, estacion, X_train, error_ini, error_train,des_ini, des_train , iteration, folder, name_file):
    
    okfile = os.path.isfile(folder + name_file)
    f = open(folder + name_file ,'a') 
    if okfile == False:
        datos = f'Tipo'+ "," +  'STATION_TRAIN'+ "," +'STATION_TEST'+ ","  + "feature"+ ","  + "resticciones"+ "," + "NumInter" + "," + "NumElem"+ "," +'mean_sts' + "," +  'mean_wrf'+ "," +'dev_sts' + "," + 'dev_train' + "," + "Mejora" + "," + 'M5_sts'+ ","  + 'M5_sts'+ ","  +'M5_wrf'+ ","  + 'M5_wrf'+ ","  + 'm5_sts'+ ","  + 'm5_sts'+ ","  +'m5_wrf'+ ","  +'m5_wrf'+ ","  + 'M10_sts'+ ","  + 'M10_sts'+ ","  + 'M10_wrf'+ ","  + 'M10_wrf'+ ","  + 'm10_sts'+ ","  + 'm10_sts'+ ","  + 'm10_wrf'+ ","  + 'm10_wrf'+ ","  + 'M15_sts'+ ","  + 'M15_sts'+ ","  + 'M15_wrf'+ ","  + 'M15_wrf'+ ","  + 'm15_sts'+ ","  + 'm15_sts'+ ","  + 'm15_wrf'+ ","  + 'm15_wrf'+ "," 
        f.write(datos)
        f.write(f"\n" )
    datos = f'{tipo}_PRUEBA'+  "," +str(estacion_train)+ "," + str(estacion)+ "," +  str(feature) + "," +  str(resticciones) + "," +  str(iteration)  + "," + str(X_train.shape[0])+ ","
    f.write(datos)
    datos =str(round(error_ini.mean(),2))+ "," + str(round(error_train.mean(),2))+ "," 
    f.write(datos)
    datos =str(round(des_ini,2))+ "," +   str(round(des_train,2))  + "," +  f"{100-round(100*des_train/des_ini,0)}"+ ","   
    f.write(datos)
    datos =  str(len(error_ini[error_ini > 5]))+ "," + str(round(len(error_ini[error_ini > 5])*100/X_train.shape[0],2)) + "," 
    f.write(datos)
    datos = str(len(error_train[error_train > 5]))+ ","  + str(round(len(error_train[error_train > 5])*100/X_train.shape[0],2)) + "," 
    f.write(datos)  
    datos =  str(len(error_ini[error_ini <-5]))+ "," + str(round(len(error_ini[error_ini <-5])*100/X_train.shape[0],2)) + "," 
    f.write(datos)
    datos =  str(len(error_train[error_train <-5]))+ ","  + str(round(len(error_train[error_train < -5])*100/X_train.shape[0],2)) + "," 
    f.write(datos)
    datos = str(len(error_ini[error_ini > 10]))+ ","  + str(round(len(error_ini[error_ini > 10])*100/X_train.shape[0],2)) + "," 
    f.write(datos)
    datos = str(len(error_train[error_train > 10]))+ ","  + str(round(len(error_train[error_train > 10])*100/X_train.shape[0],2)) + ","   
    f.write(datos)
    datos = str(len(error_ini[error_ini <-10]))+ "," + str(round(len(error_ini[error_ini <-10])*100/X_train.shape[0],2)) + "," 
    f.write(datos)
    datos = str(len(error_train[error_train <-10]))+ ","  + str(round(len(error_train[error_train < -10])*100/X_train.shape[0],2)) + ","  
    f.write(datos)
    datos = str(len(error_ini[error_ini > 15]))+ ","  + str(round(len(error_ini[error_ini > 15])*100/X_train.shape[0],2)) + "," 
    f.write(datos)
    datos = str(len(error_train[error_train > 15]))+ ","  + str(round(len(error_train[error_train > 15])*100/X_train.shape[0],2)) + ","   
    f.write(datos)
    datos = str(len(error_ini[error_ini <-15]))+ "," + str(round(len(error_ini[error_ini <-15])*100/X_train.shape[0],2)) + ","  
    f.write(datos)
    datos = str(len(error_train[error_train <-15]))+ "," + str(round(len(error_train[error_train < -15])*100/X_train.shape[0],2)) + ","
    f.write(datos)
    f.write(f"\n" )
    f.close() 

def ResultCombinacion2(tipo, feature, resticciones, estacion_train, estacion, X_train, error_ini, error_train,des_ini, des_train, pendiente, iteration, folder, name_file):
    
    okfile = os.path.isfile(folder + name_file)
    f = open(folder + name_file ,'a') 
    if okfile == False:
        datos = f'Tipo'+ "," +  'STATION_TRAIN'+ "," +'STATION_TEST'+ ","  + "feature"+ ","  + "resticciones"+ "," + "NumInter" + "," + "NumElem"+ "," +'mean_sts' + "," +  'mean_wrf'+ "," +'dev_sts' + "," + 'dev_train' + "," + "Mejora" + "," + 'M5_sts'+ ","  + 'M5_sts'+ ","  +'M5_wrf'+ ","  + 'M5_wrf'+ ","  + 'm5_sts'+ ","  + 'm5_sts'+ ","  +'m5_wrf'+ ","  +'m5_wrf'+ ","  + 'M10_sts'+ ","  + 'M10_sts'+ ","  + 'M10_wrf'+ ","  + 'M10_wrf'+ ","  + 'm10_sts'+ ","  + 'm10_sts'+ ","  + 'm10_wrf'+ ","  + 'm10_wrf'+ ","  + 'M15_sts'+ ","  + 'M15_sts'+ ","  + 'M15_wrf'+ ","  + 'M15_wrf'+ ","  + 'm15_sts'+ ","  + 'm15_sts'+ ","  + 'm15_wrf'+ ","  + 'm15_wrf'+ "," 
        f.write(datos)
        f.write(f"\n" )
    datos = f'{tipo}_PRUEBA'+  "," +str(estacion_train)+ "," + str(estacion)+ "," +  str(feature) + "," +  str(resticciones) + "," +  str(iteration)  + "," + str(X_train.shape[0])+ ","
    f.write(datos)
    datos =str(round(error_ini.mean(),2))+ "," + str(round(error_train.mean(),2))+ "," 
    f.write(datos)
    datos =str(round(des_ini,2))+ "," +   str(round(des_train,2))  + "," +  f"{100-round(100*des_train/des_ini,0)}"+ ","   
    f.write(datos)
    datos =  str(len(error_ini[error_ini > 5]))+ "," + str(round(len(error_ini[error_ini > 5])*100/X_train.shape[0],2)) + "," 
    f.write(datos)
    datos = str(len(error_train[error_train > 5]))+ ","  + str(round(len(error_train[error_train > 5])*100/X_train.shape[0],2)) + "," 
    f.write(datos)  
    datos =  str(len(error_ini[error_ini <-5]))+ "," + str(round(len(error_ini[error_ini <-5])*100/X_train.shape[0],2)) + "," 
    f.write(datos)
    datos =  str(len(error_train[error_train <-5]))+ ","  + str(round(len(error_train[error_train < -5])*100/X_train.shape[0],2)) + "," 
    f.write(datos)
    datos = str(len(error_ini[error_ini > 10]))+ ","  + str(round(len(error_ini[error_ini > 10])*100/X_train.shape[0],2)) + "," 
    f.write(datos)
    datos = str(len(error_train[error_train > 10]))+ ","  + str(round(len(error_train[error_train > 10])*100/X_train.shape[0],2)) + ","   
    f.write(datos)
    datos = str(len(error_ini[error_ini <-10]))+ "," + str(round(len(error_ini[error_ini <-10])*100/X_train.shape[0],2)) + "," 
    f.write(datos)
    datos = str(len(error_train[error_train <-10]))+ ","  + str(round(len(error_train[error_train < -10])*100/X_train.shape[0],2)) + ","  
    f.write(datos)
    datos = str(len(error_ini[error_ini > 15]))+ ","  + str(round(len(error_ini[error_ini > 15])*100/X_train.shape[0],2)) + "," 
    f.write(datos)
    datos = str(len(error_train[error_train > 15]))+ ","  + str(round(len(error_train[error_train > 15])*100/X_train.shape[0],2)) + ","   
    f.write(datos)
    datos = str(len(error_ini[error_ini <-15]))+ "," + str(round(len(error_ini[error_ini <-15])*100/X_train.shape[0],2)) + ","  
    f.write(datos)
    datos = str(len(error_train[error_train <-15]))+ "," + str(round(len(error_train[error_train < -15])*100/X_train.shape[0],2)) + ","
    f.write(datos)
    datos = str(pendiente) + ","
    f.write(datos)
    f.write(f"\n" )
    f.close() 

def articulo_tabla(dataset, folder, name_file, PREDICT):    


    
    okfile = os.path.isfile(folder + name_file+'.txt')
    f = open(folder + name_file +'.txt' ,'a') 
    if okfile == False:
        datos = f'Tipo'+ "," +  'mean_error_ini'+ "," +'mean_error_train'+ ","  + "mean_des_ini"+ ","  + "mean_des_train"+ "," + "%mejora" + "," + "%_elem_same"+ "," +'%_elem_best' + "," +  '%_elem_bad'+ "," + 'RMSE_wrf' + "," + 'RMSE_pred'
        f.write(datos)
        f.write(f"\n" )
    
    dataset2 = dataset
    Mejora = dataset2.query("wind_error_diff > 1")
    Peor =   dataset2.query("wind_error_diff <-1")
    igual =  dataset2.query("wind_error_diff >=-1 & wind_error_diff <=1")
    std_ini2 = dataset2['wind_error'].to_numpy()
    std_train2 = dataset2['wind_error_corr'].to_numpy()
    mean_ini = dataset2['wind_error'].mean()
    mean_train = dataset2['wind_error_corr'].mean()
    des_st_ini = np.sum(np.square(std_ini2))/len(std_ini2)
    des_st_train = np.sum(np.square(std_train2))/len(std_train2)
    
    datos = f'total'+  "," +str(round(dataset2['wind_error'].mean(),2))+ "," + str(round(dataset2['wind_error_corr'].mean(),2))+ "," +  str(des_st_ini) + "," +  str(des_st_train) + "," +  str(100-round(100*des_st_train/des_st_ini,0))  + "," + str(round(len(igual)*100/len(dataset2),0))  + "," + str(round(len(Mejora)*100/len(dataset2),0)) + "," + str(round(len(Peor)*100/len(dataset2),0)) + "," + str(mean_squared_error(dataset2['sts_w'], dataset2['wrf_w'], squared = False)) + "," + str(mean_squared_error(dataset2['sts_w'], dataset2[PREDICT], squared = False)) + ","
    f.write(datos)
    f.write(f"\n" )

    dataset2 = dataset.query("sts_w < 8")
    Mejora = dataset2.query("wind_error_diff > 1")
    Peor =   dataset2.query("wind_error_diff <-1")
    igual =  dataset2.query("wind_error_diff >=-1 & wind_error_diff <=1")
    std_ini2 = dataset2['wind_error'].to_numpy()
    std_train2 = dataset2['wind_error_corr'].to_numpy()
    mean_ini = dataset2['wind_error'].mean()
    mean_train = dataset2['wind_error_corr'].mean()
    des_st_ini = np.sum(np.square(std_ini2))/len(std_ini2)
    des_st_train = np.sum(np.square(std_train2))/len(std_train2)
    if len(dataset2) >0:
        datos = f'<=8'+  "," +str(round(dataset2['wind_error'].mean(),2))+ "," + str(round(dataset2['wind_error_corr'].mean(),2))+ "," +  str(des_st_ini) + "," +  str(des_st_train) + "," +  str(100-round(100*des_st_train/des_st_ini,0))  + "," + str(round(len(igual)*100/len(dataset2),0))  + "," + str(round(len(Mejora)*100/len(dataset2),0)) + "," + str(round(len(Peor)*100/len(dataset2),0)) + "," + str(mean_squared_error(dataset2['sts_w'], dataset2['wrf_w'], squared = False))+ "," + str(mean_squared_error(dataset2['sts_w'], dataset2[PREDICT], squared = False)) + ","  
        f.write(datos)
        f.write(f"\n" )

    dataset2 = dataset.query("sts_w >= 8 & sts_w <=10")
    Mejora = dataset2.query("wind_error_diff > 1")
    Peor =   dataset2.query("wind_error_diff <-1")
    igual =  dataset2.query("wind_error_diff >=-1 & wind_error_diff <=1")
    std_ini2 = dataset2['wind_error'].to_numpy()
    std_train2 = dataset2['wind_error_corr'].to_numpy()
    mean_ini = dataset2['wind_error'].mean()
    mean_train = dataset2['wind_error_corr'].mean()
    
    des_st_ini = np.sum(np.square(std_ini2))/len(std_ini2)
    des_st_train = np.sum(np.square(std_train2))/len(std_train2)
    
    if len(dataset2) >0:
        datos = f'>=8 and =<10'+  "," +str(round(dataset2['wind_error'].mean(),2))+ "," + str(round(dataset2['wind_error_corr'].mean(),2))+ "," +  str(des_st_ini) + "," +  str(des_st_train) + "," +  str(100-round(100*des_st_train/des_st_ini,0))  + "," + str(round(len(igual)*100/len(dataset2),0))   + "," + str(round(len(Mejora)*100/len(dataset2),0)) + "," + str(round(len(Peor)*100/len(dataset2),0)) + "," + str(mean_squared_error(dataset2['sts_w'], dataset2['wrf_w'], squared = False))+ "," + str(mean_squared_error(dataset2['sts_w'], dataset2[PREDICT], squared = False)) + "," 
        f.write(datos)
        f.write(f"\n" )
        
    dataset2 = dataset.query("sts_w > 10")
    Mejora = dataset2.query("wind_error_diff > 1")
    Peor =   dataset2.query("wind_error_diff <-1")
    igual =  dataset2.query("wind_error_diff >=-1 & wind_error_diff <=1")
    std_ini2 = dataset2['wind_error'].to_numpy()
    std_train2 = dataset2['wind_error_corr'].to_numpy()
    mean_ini = dataset2['wind_error'].mean()
    mean_train = dataset2['wind_error_corr'].mean()
    des_st_ini = np.sum(np.square(std_ini2))/len(std_ini2)
    des_st_train = np.sum(np.square(std_train2))/len(std_train2)
    if len(dataset2) >0:
        datos = f'>10'+  "," +str(round(dataset2['wind_error'].mean(),2))+ "," + str(round(dataset2['wind_error_corr'].mean(),2))+ "," +  str(des_st_ini) + "," +  str(des_st_train) + "," +  str(100-round(100*des_st_train/des_st_ini,0))  + "," + str(round(len(igual)*100/len(dataset2),0))  + "," + str(round(len(Mejora)*100/len(dataset2),0)) + "," + str(round(len(Peor)*100/len(dataset2),0)) + "," + str(mean_squared_error(dataset2['sts_w'], dataset2['wrf_w'], squared = False))+ "," + str(mean_squared_error(dataset2['sts_w'], dataset2[PREDICT], squared = False)) + "," 
        f.write(datos)
        f.write(f"\n" )
    f.close() 
    print(folder +name_file + '.txt')

def articulo_error(dataset, folder, name_file, PREDICT):    


    
    okfile = os.path.isfile(folder + name_file +'.txt')
    f = open(folder + name_file +'.txt' ,'a') 
    if okfile == False:
        datos = 'Tipo' + ';' + 'corr_wrf'+ ';'+ 'corr_pred' + ';' + 'error_sobre_wrf' + ';' + 'error_sub_wrf' + ';' + 'error_igual_wrf' + ';' + 'error_sobre_pred' + ';' + 'error_sub_pred' + ';'  + 'error_total_wrf' +  ';'  + 'error_total_pred' + ';' + 'Empeoramiento_respecto_wrf_wrf' + ';' + 'Mejor_respecto_wrf_wrf' + ';' + 'Empeoramiento_respecto_wrf_pred' + ';' + 'Mejor_respecto_wrf_pred' 
        #print(datos)
        f.write(datos)
        f.write(f"\n" )
    
    dataset2 = dataset
    Mejora = dataset2.query("wind_error_diff > 1")
    Peor =   dataset2.query("wind_error_diff <-1")
    error_sobre = dataset2.query("wind_error_corr > 1")
    error_sobre_wrf = dataset2.query("wind_error > 1")
    error_sub =  dataset2.query("wind_error_corr < -1")
    error_sub_wrf =  dataset2.query("wind_error < -1")
    error_igual = dataset2.query(" wind_error_corr <= 1 & wind_error_corr >= -1")

    error_sobre['er_re_pred'] = abs(error_sobre['wind_error_corr'])*100 / (error_sobre['sts_w']+1)
    error_sobre_wrf['er_re_wrf'] = abs(error_sobre_wrf['wind_error'])*100 / (error_sobre_wrf['sts_w']+1)
    error_sub['er_re_pred'] = abs(error_sub['wind_error_corr'])*100 / (error_sub['sts_w']+1)
    error_sub_wrf['er_re_wrf'] = abs(error_sub_wrf['wind_error'])*100 / (error_sub_wrf['sts_w']+1)   
    error_igual['er_re_pred'] = abs(error_igual['wind_error_corr'])*100 / (error_igual['sts_w']+1)
    error_igual['er_re_wrf'] = abs(error_igual['wind_error'])*100 / (error_igual['sts_w']+1)  
    dataset2['er_re_pred'] = abs(dataset2['wind_error_corr'])*100 / (dataset2['sts_w']+1)
    dataset2['er_re_wrf'] = abs(dataset2['wind_error'])*100 / (dataset2['sts_w']+1)  
    Mejora['er_re_pred'] = abs(Mejora['wind_error_diff'])*100 / (Mejora['wrf_w']+1)
    Mejora['er_re_wrf'] = abs(Mejora['wind_error_diff'])*100 / (Mejora['wrf_w']+1)  
    Peor['er_re_pred'] = abs(Peor['wind_error_diff'])*100 / (Peor['wrf_w']+1)
    Peor['er_re_wrf'] = abs(Peor['wind_error_diff'])*100 / (Peor['wrf_w']+1) 

    datos = f'total'+  ';'+str(dataset2['sts_w'].corr(dataset2['wrf_w'], method='pearson')) +';' + str(dataset2['sts_w'].corr(dataset2[PREDICT], method='pearson')) 
    f.write(datos)
    datos =  str(error_sobre_wrf['er_re_wrf'].mean()) + ';' + str(error_sub_wrf['er_re_wrf'].mean()) + ';' + str(error_igual['er_re_wrf'].mean()) + ';' + str(error_igual['er_re_wrf'].mean()) + ';' + str(error_sobre['er_re_pred'].mean()) + ';' + str(error_sub['er_re_pred'].mean()) + ';' + str(dataset2['er_re_wrf'].mean()) + ';' + str(dataset2['er_re_pred'].mean()) +  ';' + str(Mejora['er_re_wrf'].mean()) + ';' + str(Mejora['er_re_pred'].mean()) +  ';' + str(Peor['er_re_wrf'].mean()) + ';' + str(Peor['er_re_pred'].mean())
    f.write(datos)
    
    f.write(f"\n" )

    dataset2 = dataset.query("sts_w < 8")
    if len(dataset2) >0:
        Mejora = dataset2.query("wind_error_diff > 1")
        Peor =   dataset2.query("wind_error_diff <-1")
        error_sobre = dataset2.query("wind_error_corr > 1")
        error_sobre_wrf = dataset2.query("wind_error > 1")
        error_sub =  dataset2.query("wind_error_corr < -1")
        error_sub_wrf =  dataset2.query("wind_error < -1")
        error_igual = dataset2.query(" wind_error_corr <= 1 & wind_error_corr >= -1")

        error_sobre['er_re_pred'] = abs(error_sobre['wind_error_corr'])*100 / (error_sobre['sts_w']+0.01)
        error_sobre_wrf['er_re_wrf'] = abs(error_sobre_wrf['wind_error'])*100 / (error_sobre_wrf['sts_w']+0.01)
        error_sub['er_re_pred'] = abs(error_sub['wind_error_corr'])*100 / (error_sub['sts_w']+0.01)
        error_sub_wrf['er_re_wrf'] = abs(error_sub_wrf['wind_error'])*100 / (error_sub_wrf['sts_w']+0.01)   
        error_igual['er_re_pred'] = abs(error_igual['wind_error_corr'])*100 / (error_igual['sts_w']+0.01)
        error_igual['er_re_wrf'] = abs(error_igual['wind_error'])*100 / (error_igual['sts_w']+0.01)  
        dataset2['er_re_pred'] = abs(dataset2['wind_error_corr'])*100 / (dataset2['sts_w']+0.01)
        dataset2['er_re_wrf'] = abs(dataset2['wind_error'])*100 / (dataset2['sts_w']+0.01)  
        Mejora['er_re_pred'] = abs(Mejora['wind_error_diff'])*100 / (Mejora['wrf_w']+0.01)
        Mejora['er_re_wrf'] = abs(Mejora['wind_error_diff'])*100 / (Mejora['wrf_w']+0.01)  
        Peor['er_re_pred'] = abs(Peor['wind_error_diff'])*100 / (Peor['wrf_w']+0.01)
        Peor['er_re_wrf'] = abs(Peor['wind_error_diff'])*100 / (Peor['wrf_w']+0.01) 
        
        datos = f'<8'+  ';'+str(dataset2['sts_w'].corr(dataset2['wrf_w'], method='pearson')) +';' + str(dataset2['sts_w'].corr(dataset2[PREDICT], method='pearson')) +';' + str(error_sobre_wrf['er_re_wrf'].mean()) + ';' + str(error_sub_wrf['er_re_wrf'].mean()) + ';' + str(error_igual['er_re_wrf'].mean()) + ';' + str(error_igual['er_re_wrf'].mean()) + ';' + str(error_sobre['er_re_pred'].mean()) + ';' + str(error_sub['er_re_pred'].mean()) + ';' + str(dataset2['er_re_wrf'].mean()) + ';' + str(dataset2['er_re_pred'].mean()) +  ';' + str(Mejora['er_re_wrf'].mean()) + ';' + str(Mejora['er_re_pred'].mean()) +  ';' + str(Peor['er_re_wrf'].mean()) + ';' + str(Peor['er_re_pred'].mean())
        f.write(datos)
        f.write(f"\n" )


    dataset2 = dataset.query("sts_w >= 8 & sts_w <=10")
    if len(dataset2) >0:
        Mejora = dataset2.query("wind_error_diff > 1")
        Peor =   dataset2.query("wind_error_diff <-1")
        error_sobre = dataset2.query("wind_error_corr > 1")
        error_sobre_wrf = dataset2.query("wind_error > 1")
        error_sub =  dataset2.query("wind_error_corr < -1")
        error_sub_wrf =  dataset2.query("wind_error < -1")
        error_igual = dataset2.query(" wind_error_corr <= 1 & wind_error_corr >= -1")

        error_sobre['er_re_pred'] = abs(error_sobre['wind_error_corr'])*100 / (error_sobre['sts_w']+0.01)
        error_sobre_wrf['er_re_wrf'] = abs(error_sobre_wrf['wind_error'])*100 / (error_sobre_wrf['sts_w']+0.01)
        error_sub['er_re_pred'] = abs(error_sub['wind_error_corr'])*100 / (error_sub['sts_w']+0.01)
        error_sub_wrf['er_re_wrf'] = abs(error_sub_wrf['wind_error'])*100 / (error_sub_wrf['sts_w']+0.01)   
        error_igual['er_re_pred'] = abs(error_igual['wind_error_corr'])*100 / (error_igual['sts_w']+0.01)
        error_igual['er_re_wrf'] = abs(error_igual['wind_error'])*100 / (error_igual['sts_w']+0.01)  
        dataset2['er_re_pred'] = abs(dataset2['wind_error_corr'])*100 / (dataset2['sts_w']+0.01)
        dataset2['er_re_wrf'] = abs(dataset2['wind_error'])*100 / (dataset2['sts_w']+0.01)  
        Mejora['er_re_pred'] = abs(Mejora['wind_error_diff'])*100 / (Mejora['wrf_w']+0.01)
        Mejora['er_re_wrf'] = abs(Mejora['wind_error_diff'])*100 / (Mejora['wrf_w']+0.01)  
        Peor['er_re_pred'] = abs(Peor['wind_error_diff'])*100 / (Peor['wrf_w']+0.01)
        Peor['er_re_wrf'] = abs(Peor['wind_error_diff'])*100 / (Peor['wrf_w']+0.01) 
        
        datos = f'>=8 and =<10'+  ';'+str(dataset2['sts_w'].corr(dataset2['wrf_w'], method='pearson')) +';' + str(dataset2['sts_w'].corr(dataset2[PREDICT], method='pearson')) +';' + str(error_sobre_wrf['er_re_wrf'].mean()) + ';' + str(error_sub_wrf['er_re_wrf'].mean()) + ';' + str(error_igual['er_re_wrf'].mean()) + ';' + str(error_igual['er_re_wrf'].mean()) + ';' + str(error_sobre['er_re_pred'].mean()) + ';' + str(error_sub['er_re_pred'].mean()) + ';' + str(dataset2['er_re_wrf'].mean()) + ';' + str(dataset2['er_re_pred'].mean()) +  ';' + str(Mejora['er_re_wrf'].mean()) + ';' + str(Mejora['er_re_pred'].mean()) +  ';' + str(Peor['er_re_wrf'].mean()) + ';' + str(Peor['er_re_pred'].mean())
        f.write(datos)
        f.write(f"\n" )


        
    dataset2 = dataset.query("sts_w > 10")
    if len(dataset2) >0:
        Mejora = dataset2.query("wind_error_diff > 1")
        Peor =   dataset2.query("wind_error_diff <-1")
        error_sobre = dataset2.query("wind_error_corr > 1")
        error_sobre_wrf = dataset2.query("wind_error > 1")
        error_sub =  dataset2.query("wind_error_corr < -1")
        error_sub_wrf =  dataset2.query("wind_error < -1")
        error_igual = dataset2.query(" wind_error_corr <= 1 & wind_error_corr >= -1")

        error_sobre['er_re_pred'] = abs(error_sobre['wind_error_corr'])*100 / (error_sobre['sts_w']+0.01)
        error_sobre_wrf['er_re_wrf'] = abs(error_sobre_wrf['wind_error'])*100 / (error_sobre_wrf['sts_w']+0.01)
        error_sub['er_re_pred'] = abs(error_sub['wind_error_corr'])*100 / (error_sub['sts_w']+0.01)
        error_sub_wrf['er_re_wrf'] = abs(error_sub_wrf['wind_error'])*100 / (error_sub_wrf['sts_w']+0.01)   
        error_igual['er_re_pred'] = abs(error_igual['wind_error_corr'])*100 / (error_igual['sts_w']+0.01)
        error_igual['er_re_wrf'] = abs(error_igual['wind_error'])*100 / (error_igual['sts_w']+0.01)  
        dataset2['er_re_pred'] = abs(dataset2['wind_error_corr'])*100 / (dataset2['sts_w']+0.01)
        dataset2['er_re_wrf'] = abs(dataset2['wind_error'])*100 / (dataset2['sts_w']+0.01)  
        Mejora['er_re_pred'] = abs(Mejora['wind_error_diff'])*100 / (Mejora['wrf_w']+0.01)
        Mejora['er_re_wrf'] = abs(Mejora['wind_error_diff'])*100 / (Mejora['wrf_w']+0.01)  
        Peor['er_re_pred'] = abs(Peor['wind_error_diff'])*100 / (Peor['wrf_w']+0.01)
        Peor['er_re_wrf'] = abs(Peor['wind_error_diff'])*100 / (Peor['wrf_w']+0.01) 
        
        datos = f'>10'+  ';'+str(dataset2['sts_w'].corr(dataset2['wrf_w'], method='pearson')) +';' + str(dataset2['sts_w'].corr(dataset2[PREDICT], method='pearson')) +';' + str(error_sobre_wrf['er_re_wrf'].mean()) + ';' + str(error_sub_wrf['er_re_wrf'].mean()) + ';' + str(error_igual['er_re_wrf'].mean()) + ';' + str(error_igual['er_re_wrf'].mean()) + ';' + str(error_sobre['er_re_pred'].mean()) + ';' + str(error_sub['er_re_pred'].mean()) + ';' + str(dataset2['er_re_wrf'].mean()) + ';' + str(dataset2['er_re_pred'].mean()) +  ';' + str(Mejora['er_re_wrf'].mean()) + ';' + str(Mejora['er_re_pred'].mean()) +  ';' + str(Peor['er_re_wrf'].mean()) + ';' + str(Peor['er_re_pred'].mean())
        f.write(datos)
        f.write(f"\n" )




def articulo_graf(error_ini, error_train, dev_ini, dev_train, des_st_ini, des_st_train, dataset, PREDICT, COMPARE, TARGET, folder, name_file):    
    


    dataset['wind_error'] = error_ini
    dataset['wind_error_corr'] = error_train
    dataset['wind_error_diff'] = abs(error_ini) - abs(error_train)

    articulo_tabla(dataset, folder, name_file, PREDICT)
    articulo_error(dataset, folder, name_file + '_error', PREDICT)

 

    fig, ((ax1, ax2, ax3, ax4), (ax5,ax6,ax7,ax8), (ax9,ax10,ax11,ax12)) = plt.subplots(3, 4, figsize=(9, 7))

    #print("Mean:",mean_st,", Std:",dev_st)
    

    xmin = -15
    xmax = 15
    mybins = 100
    #dataset.index = dataset.reset_index()
    '''
    ax1 = sns.kdeplot(data=dataset, x="wind_error", color='blue', label = 'wrf-sts error')
    ax1 = sns.kdeplot(dataset['wind_error_corr'], color='red', label = 'prd-sts error')
    ax1.legend(fontsize=5)
    ax1.set_xlabel("Error of wind speed in station (mph)")
    ax1.set_ylim([0, 0.4])
    ax1.set_xlim([xmin, xmax])
    '''
    ax1.hist(error_ini, range=(xmin, xmax), bins=mybins, color='blue', histtype='step', density=True, label = 'wrf-sts')
    ax1.hist(error_train, range=(xmin, xmax), bins=mybins, color='red', histtype='step', density=True, label = 'prd-sts')
    ax1.set_title("All data (1)")
    ax1.set_xlabel('error (mph)')
    ax1.set_ylabel('density (a)')
    ax1.legend(fontsize=5)
    ax1.set_ylim([0, 0.4])
    
    ax2.hist(dataset[dataset['sts_w'] < 8]['wind_error'].values, range=(xmin, xmax), bins=mybins, color='royalblue', histtype='step', density=True, label = '(<8) wrf-sts')
    ax2.hist(dataset[dataset['sts_w'] < 8]['wind_error_corr'].values, range=(xmin, xmax), bins=mybins, color='deeppink', histtype='step', density=True, label = '(<8) prd-sts')
    ax2.set_title("< 8 sts (2)")
    ax2.set_xlabel('error (mph)')
    ax2.legend(fontsize=5)
    ax2.set_ylim([0, 0.4])
    
    dataset3 = dataset.query("sts_w >= 8 & sts_w <=10")
    ax3.hist(dataset3['wind_error'].values, range=(xmin, xmax), bins=mybins, color='deepskyblue', histtype='step', density=True, label = '(>8) wrf-sts')
    ax3.hist(dataset3['wind_error_corr'].values, range=(xmin, xmax), bins=mybins, color='lightcoral', histtype='step', density=True, label = '(>8) prd-sts')
    ax3.legend(fontsize=5)
    ax3.set_xlabel('error (mph)')
    ax3.set_title(">= 8 & <=10 sts (3)")
    ax3.set_ylim([0, 0.4])
    
    ax4.hist(dataset[dataset['sts_w'] > 10]['wind_error'].values, range=(xmin, xmax), bins=mybins, color='dodgerblue', histtype='step', density=True, label = '(>10) wrf-sts')
    ax4.hist(dataset[dataset['sts_w'] > 10 ]['wind_error_corr'].values, range=(xmin, xmax), bins=mybins, color='magenta', histtype='step', density=True, label = '(>10) prd-sts')
    ax4.set_title(">10 sts (4)")
    ax4.set_xlabel('error (mph)')
    ax4.legend(fontsize=5)
    ax4.set_ylim([0, 0.4])

    xmin = -10
    xmax = 10
    mybins = 100

    dataset2 = dataset
    Mejora = dataset2.query("wind_error_diff > 1")
    Peor =   dataset2.query("wind_error_diff <-1")

    ax5.hist(dataset['wind_error_diff'], bins=mybins, color='blue', range=(xmin, xmax), histtype='step', density=True, cumulative=False)
    ax5.set_ylim([0, 0.5])
    ax5.axvspan(-10,-1, color = 'r',alpha = 0.15) 
    ax5.axvspan(1,10, color = 'g',alpha = 0.15)
    ax5.set_xlabel('Pred. status (mph)') 
    ax5.set_ylabel('density (b)')
    ax5.text(-9, 0.45, f'{str(round(len(Peor)*100/len(dataset2),0))}%')
    ax5.text(2, 0.45, f'{str(round(len(Mejora)*100/len(dataset2),0))}%')
    #plt.axhspan(-0.5,0.5, alpha = 0.25)  #  Dibujamos un recuadro azul horizontal entre x[-0.5,0.5] con transparencia 0.25
    #plt.text(60, .025, r'$\mu=100,\ \sigma=15$')

    dataset2 = dataset.query("sts_w < 8")
    Mejora = dataset2.query("wind_error_diff > 1")
    Peor =   dataset2.query("wind_error_diff <-1")
    ax6.hist(dataset[dataset['sts_w'] < 8]['wind_error_diff'].values, range=(xmin, xmax), bins=mybins, color='royalblue', histtype='step', density=True, cumulative=False)
    ax6.set_ylim([0, 0.5])
    ax6.axvspan(-10,-1, color = 'r',alpha = 0.15) 
    ax6.axvspan(1,10, color = 'g',alpha = 0.15) 
    ax6.set_xlabel('Pred. status (mph)')
    
    ax6.text(-9, 0.45, f'{str(round(len(Peor)*100/len(dataset2),0))}%')
    ax6.text(2, 0.45, f'{str(round(len(Mejora)*100/len(dataset2),0))}%')

    dataset2 = dataset.query("sts_w >= 8 & sts_w <=10")
    Mejora = dataset2.query("wind_error_diff > 1")
    Peor =   dataset2.query("wind_error_diff <-1")    
    ax7.hist(dataset2['wind_error_diff'].values, range=(xmin, xmax), bins=mybins, color='royalblue', histtype='step', density=True, cumulative=False)
    ax7.set_ylim([0, 0.5])
    ax7.set_xlabel('Pred. status (mph)')
    ax7.axvspan(-10,-1, color = 'r',alpha = 0.15) 
    ax7.axvspan(1,10, color = 'g',alpha = 0.15) 
    if len(dataset2) >0:
        ax7.text(-9, 0.45, f'{str(round(len(Peor)*100/len(dataset2),0))}%')
        ax7.text(2, 0.45, f'{str(round(len(Mejora)*100/len(dataset2),0))}%')
    else:
        ax7.text(-9, 0.45, f'0%')
        ax7.text(2, 0.45, f'0%')


    dataset2 = dataset.query("sts_w > 10")
    Mejora = dataset2.query("wind_error_diff > 1")
    Peor =   dataset2.query("wind_error_diff <-1")       
    ax8.hist(dataset[dataset['sts_w'] > 10]['wind_error_diff'].values, range=(xmin, xmax), bins=mybins, color='royalblue', histtype='step', density=True, cumulative=False)
    ax8.set_ylim([0, 0.5])
    ax8.set_xlabel('Pred. status (mph)')
    ax8.axvspan(-10,-1, color = 'r',alpha = 0.15) 
    ax8.axvspan(1,10, color = 'g',alpha = 0.15) 
    if len(dataset2) >0:
        ax8.text(-9, 0.45, f'{str(round(len(Peor)*100/len(dataset2),0))}%')
        ax8.text(2, 0.45, f'{str(round(len(Mejora)*100/len(dataset2),0))}%')
    else:
        ax8.text(-9, 0.45, f'0%')
        ax8.text(2, 0.45, f'0%')

    xdata = list(range(41))
    ydata = [_ for _ in xdata]

    dataset2 = dataset
    ax9.plot(xdata, ydata, 'g')
    ax9.scatter(dataset2['sts_w'], dataset2['wrf_w'], s=2,alpha=.2,color='blue')
    ax9.scatter(dataset2['sts_w'], dataset2[PREDICT], s=2,alpha=.2,color='red')
    ax9.set_xlabel('wind sts (mph)')
    ax9.set_ylabel('wind wrf/pred (mph) (c)')
    ax9.set_ylim(0,40)
    ax9.set_xlim(0,40)
    ax9.grid(True)
    
    dataset2 = dataset.query("sts_w < 8")
    ax10.plot(xdata, ydata, 'g')
    ax10.scatter(dataset2['sts_w'], dataset2['wrf_w'], s=2,alpha=.2,color='blue')
    ax10.scatter(dataset2['sts_w'], dataset2[PREDICT], s=2,alpha=.2,color='red')
    ax10.set_xlabel('wind sts (mph)')
    ax10.set_ylim(0,40)
    ax10.set_xlim(0,40)
    ax10.grid(True)

    dataset2 = dataset.query("sts_w >= 8 & sts_w <=10")
    ax11.plot(xdata, ydata, 'g')
    ax11.scatter(dataset2['sts_w'], dataset2['wrf_w'], s=2,alpha=.2,color='blue')
    ax11.scatter(dataset2['sts_w'], dataset2[PREDICT], s=2,alpha=.2,color='red')
    ax11.set_xlabel('wind sts (mph)')
    ax11.set_ylim(0,40)
    ax11.set_xlim(0,40)
    ax11.grid(True)

    dataset2 = dataset.query("sts_w > 10")
    ax12.plot(xdata, ydata, 'g')
    ax12.scatter(dataset2['sts_w'], dataset2['wrf_w'], s=2,alpha=.2,color='blue')
    ax12.scatter(dataset2['sts_w'], dataset2[PREDICT], s=2,alpha=.2,color='red')
    ax12.set_xlabel('wind sts (mph)')
    ax12.set_ylim(0,40)
    ax12.set_xlim(0,40)
    ax12.grid(True)

    plt.tight_layout()
    plt.savefig(folder +name_file + '_40.png')
    plt.clf()

    fig, ((ax1, ax2)) = plt.subplots(1, 2, figsize=(9, 7))

    #print("Mean:",mean_st,", Std:",dev_st)
    

    xmin = -15
    xmax = 15
    mybins = 100
    
    ax1.hist(dataset[dataset['sts_w'] > 15]['wind_error'].values, range=(xmin, xmax), bins=mybins, color='dodgerblue', histtype='step', density=True, label = '(>10) wrf-sts')
    ax1.hist(dataset[dataset['sts_w'] > 15 ]['wind_error_corr'].values, range=(xmin, xmax), bins=mybins, color='magenta', histtype='step', density=True, label = '(>10) prd-sts')
    #ax1.set_title(">15 sts (1)")
    ax1.set_xlabel('error (mph)')
    ax1.legend(fontsize=5)
    ax1.set_ylim([0, 0.4])

    xmin = -10
    xmax = 10
    mybins = 100

    dataset2 = dataset.query("sts_w > 15")
    Mejora = dataset2.query("wind_error_diff > 1")
    Peor =   dataset2.query("wind_error_diff <-1")       
    ax2.hist(dataset[dataset['sts_w'] > 15]['wind_error_diff'].values, range=(xmin, xmax), bins=mybins, color='royalblue', histtype='step', density=True, cumulative=False)
    ax2.set_ylim([0, 0.5])
    ax2.set_xlabel('Pred. status (mph)')
    ax2.axvspan(-10,-1, color = 'r',alpha = 0.15) 
    ax2.axvspan(1,10, color = 'g',alpha = 0.15) 
    if len(dataset2) >0:
        ax2.text(-9, 0.45, f'{str(round(len(Peor)*100/len(dataset2),0))}%')
        ax2.text(2, 0.45, f'{str(round(len(Mejora)*100/len(dataset2),0))}%')
    else:
        ax2.text(-9, 0.45, f'0%')
        ax2.text(2, 0.45, f'0%')

    plt.tight_layout()
    plt.savefig(folder +name_file + '_event.png')
    plt.clf()