
import math
import random
import pandas as pd
import numpy as np
import ML_help_
import LIBS.S_Analis_
import LIBS.S_kmeans_
from sklearn.ensemble import RandomForestRegressor
from sklearn.feature_selection import RFE
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error, r2_score
import os
import secrets
import pickle
from sklearn.metrics import mean_squared_error, r2_score
from scipy import stats
from scipy.stats import pearsonr
import joblib 
import glob
import ntpath
import datetime
from netCDF4 import Dataset
from datetime import date
from scipy import signal


inter_max = 10

def crearcarpeta(folder):

    os.makedirs(folder, exist_ok=True)

def ajuste_correccion(df,ii,hour,y_pred):
    #print('ii: ',ii, ' hour:',hour)
    if int(hour) == 0:
        df["y_pred"] = y_pred + 0.1*y_pred

    if int(hour) == 1:
        df["y_pred"] = y_pred + 0.3*y_pred

    if int(hour) == 2:
        df["y_pred"] = y_pred + 0.4*y_pred

    if int(hour) == 3:
        df["y_pred"] = y_pred + 0.1*y_pred

    if int(hour) == 4:
        df["y_pred"] = y_pred + 0.3*y_pred

    if int(hour) == 5:
        df["y_pred"] = y_pred + 0.3*y_pred

    if int(hour) == 6:
        df["y_pred"] = y_pred + 0.2*y_pred

    if int(hour) == 7:
        df["y_pred"] = y_pred + 0.2*y_pred

    if int(hour) == 8:
        df["y_pred"] = y_pred + 0.2*y_pred

    if int(hour) == 9:
        df["y_pred"] = y_pred + 0.4*y_pred

    if int(hour) == 10:
        df["y_pred"] = y_pred + 0.3*y_pred

    if int(hour) == 11:
        df["y_pred"] = y_pred + 0.4*y_pred

    if int(hour) == 12:
        df["y_pred"] = y_pred + 0.3*y_pred

    if int(hour) == 13:
        df["y_pred"] = y_pred + 0.4*y_pred

    if int(hour) == 14:
        df["y_pred"] = y_pred + 0.3*y_pred

    if int(hour) == 15:
        df["y_pred"] = y_pred + 0.4*y_pred

    if int(hour) == 16:
        df["y_pred"] = y_pred + 0.3*y_pred

    if int(hour) == 17:
        df["y_pred"] = y_pred + 0.2*y_pred

    if int(hour) == 18:
        df["y_pred"] = y_pred + 0.3*y_pred

    if int(hour) == 19:
        df["y_pred"] = y_pred + 0.2*y_pred

    if int(hour) == 20:
        df["y_pred"] = y_pred + 0.2*y_pred

    if int(hour) == 21:
        df["y_pred"] = y_pred + 0.3*y_pred

    if int(hour) == 22:
        df["y_pred"] = y_pred + 0.2*y_pred

    if int(hour) == 23:
        df["y_pred"] = y_pred + 0.1*y_pred
    #print(df)
    return df

def date_station_pge(hour,date,WORKING_FOLDER, file_station):
    
    #df = pd.read_csv(WORKING_FOLDER+f"Wind_wrf_Techno_ninja_{int(tt)}_PGE.csv")


    # Load STATION_features, select variables, prepare dummies and merge
    # ------------------------------------------------------
    #print("STATION features....")
    df = pd.read_csv(WORKING_FOLDER+"stations_grid.csv")
    df = ML_help_.Landform_dummis(df)
    #df = df.merge(df_sts, on=['MyidGridAll','ID_STATION'])
    #print(df)

    #print("STATION features....")
    #df_sts = pd.read_csv(WORKING_FOLDER+"grid.csv")
    #df_sts = ML_help_.Landform_dummis(df_sts)
    #df = df.merge(df_sts, on=['MyidGridAll'])
    #print(df)
    
    tt = int(hour)-24*int(int(hour)/24)
   
    df = df.query(f"SHORTNAME=='PGE'  & inCAL == 1")
    df =df[~df['MyidGridAll'].isin([131248, 83718, 136354, 39505, 37849, 86464, 103863, 95565, 118942, 115758, 160065, 159787, 37430, 82902, 40204, 30321, 75402, 148271, 112219, 120520, 112579, 84091, 135605, 93281, 34356, 124444, 75403, 149422, 97136, 133190, 129282, 156975, 76694, 88854, 84090, 35460, 95252, 28410, 87675, 74323, 67992, 92023, 86492, 70369, 154981, 86487, 152594, 143437, 96358, 109815, 124838, 37833, 66019, 152620, 74218, 107024, 88936, 103570, 146290, 131958, 154175, 76988, 93191, 109425, 122472, 77782, 57646, 31111, 145103, 69078, 154193, 139187, 27617, 79062, 74612, 83739, 27992, 111001, 26431, 137600, 76988, 74218, 145005, 27163, 139187, 92430, 40204, 70369, 95252, 73549])]
    
    #print(tt, 'hour')
    #print(df)
    #print("STATION Variables....")
    df_sts = pd.read_csv(WORKING_FOLDER+ file_station)
    df = df.merge(df_sts, on=['ID_STATION'])
    #print(df)
    
    #df_sts2 = pd.read_csv(WORKING_FOLDER+f"stations_2022_Nov_CLEAN.csv")
    #df_sts= pd.concat([df_sts,df_sts2], sort = False) 
    #df = df.merge(df_sts, on=['ID_STATION'])
    
    df = df[['ID_STATION','MyidGridAll','hour','day','month','year','sts_w']]
    #print(df)
    if int(hour)>=24:
        date2 = date + datetime.timedelta(days=int(hour/24))
        tt= pred1D=interpolar(pred1D)
        day= date2.strftime("%d")
        month= date2.strftime("%m")
        year= date2.strftime("%Y")
    else:
        tt = int(hour)
        day = date.strftime("%d")
        month = date.strftime("%m")
        year = date.strftime("%Y")
    df = df.query(f"hour == {int(hour)} & day =={int(day)} & month == {int(month)} & year == {year}")
    #print(df)
    return df

def interpolar(pred1D,intepr_max):
    
    for ii in range(len(pred1D)):
        #print(ii)
        if pred1D[ii] == -9999.0:
 
            count =0
            der = 0
           
            
            while ((ii+count)<len(pred1D)-1 and der ==0 and count< intepr_max):
                #print(count, ii+count)
                #print(count, ii, 'Entro derecha')
                count = count +1
                if pred1D[ii+count] != -9999.0:
                    #print(pred1D[ii+count])
                    der =1
                    der_valor = pred1D[ii+count]

            izq = 0    
            if der == 1:
                count =0
                
                while ((ii-count)>=0 and izq ==0 and count<intepr_max):
                    count = count +1
                    if pred1D[ii-count] != -9999.0:
                        izq =1
                        izq_valor = pred1D[ii-count]
            
                arr = 0
                if izq == 1: 
                    while ((ii-count*396)>=0 and arr ==0 and count<intepr_max):
                        count = count +1
                        if pred1D[ii-count*396] != -9999.0:
                            arr =1
                            arr_valor = pred1D[ii-count*396]
                    baj = 0
                    if arr == 1: 
                        while ((ii+count*396)<len(pred1D)-count*intepr_max and baj ==0 and count<intepr_max):
                            count = count +1
                            if pred1D[ii+count*396] != -9999.0:
                                baj =1
                                baj_valor = pred1D[ii+count*396]            
            
            if izq ==1 and der == 1 and baj == 1 and arr == 1:
                
                #print('izq_valor:',izq_valor, 'der_valor:', der_valor)
                pred1D[ii] = (izq_valor + der_valor + arr_valor + baj_valor)/4
                #print(pred1D[ii])
    return pred1D

def comparacionstation(df, merge,OUTPUT_FOLDER,name, hour):
    OUTPUT_FOLDER_G = OUTPUT_FOLDER + "imagen/"
    crearcarpeta(OUTPUT_FOLDER_G)
    data_pred = merge[['MyidGridAll','wrf_w','y_pred_ori','y_pred_inter']]
    df = df.merge(data_pred, on=['MyidGridAll'])
    df = df.query(f"y_pred_inter != -9999")
    #print(df[])
    if len(df)>0:
        LIBS.S_Analis_.AplotScatter(df["sts_w"].values, df["wrf_w"].values, df['y_pred_inter'].values, OUTPUT_FOLDER_G , f"comp_station_{name}_{hour}.png")
        OUTPUT_FOLDER_G = OUTPUT_FOLDER + "Data/"
        crearcarpeta(OUTPUT_FOLDER_G)
        df = df[['MyidGridAll', 'day', 'month', 'year','hour','wrf_w','y_pred_ori','y_pred_inter','sts_w']]
        df.to_csv(OUTPUT_FOLDER_G+ f'Data_{hour}_{name}_.csv') 


def CreateNetcdf_f(INPUT_FOLDER, OUTPUT_FOLDER, RESULT_FOLDER, hour, hour_ini, hour_fin,path,file,file_station,comp_station,auto_mean,conv,interp, name_out):
    if int(hour) >=24:
        real_hour = hour
        hour = int(hour) -24
    else:
        real_hour = hour
    crearcarpeta(OUTPUT_FOLDER) 
    listOfDF=[]
    listOfWRF=[]
    listOfrt2D=[]
    wrfName=ntpath.basename(file)
    file_in =INPUT_FOLDER + wrfName 
    file_out =OUTPUT_FOLDER+name_out+'_'+wrfName[:-3]+f"_techno_rt_.nc" 
    
    
    date=datetime.datetime.strptime(wrfName, 'Techno_wrfsfc_d03_%Y%m%d_0000.nc')
    fecha_ar = date.strftime("%d%m%Y")   
    
    with Dataset(file,'r') as ncin:
        for ii in range(hour_ini, hour_fin+1):
            wrf=pd.DataFrame()
            _u=ncin.variables["U10"][ii,:,:]*(1/10)*0.621371
            _u = np.reshape( _u, -1)
            _v=ncin.variables["V10"][ii,:,:]*(1/10)*0.621371
            _v = np.reshape( _v, -1)
            wrf[f'wrf_u'] =_u.round(2)
            wrf[f'wrf_v'] =_v.round(2)
            null_columns=wrf.columns[wrf.isnull().any()]
            wrf[null_columns].isnull().sum()
            wrf = wrf.dropna(how='any')
            null_columns=wrf.columns[wrf.isnull().any()]
            wrf[null_columns].isnull().sum()  
            wrf[f'wrf_w']= round(np.sqrt(wrf[f'wrf_u'].pow(2) + wrf[f'wrf_v'].pow(2)),2)
            wrf[f'wrf_dir']= round((180.0/math.pi) * np.arctan2(wrf[f'wrf_u'], wrf[f'wrf_v']),2)
            wrf['MyidGridAll'] =wrf.index

            if ii>=24:
                date2 = date + datetime.timedelta(days=int(ii/24))
                wrf['hour']= ii-int(24*int(ii/24))
                hour_work = ii-int(24*int(ii/24))
                wrf['day']= date2.strftime("%d")
                wrf['month']= date2.strftime("%m")
                wrf['year']= date2.strftime("%Y")
            else:
                wrf['hour']= ii
                hour_work = ii
                wrf['day']= date.strftime("%d")
                wrf['month']= date.strftime("%m")
                wrf['year']= date.strftime("%Y")
            


            fecha_ar = date.strftime("%d%m%Y")
            results=pd.read_csv(RESULT_FOLDER+f'Predict_Result_{hour_work}_{fecha_ar}_.csv')#, index_col=[0])
        
            r=results[['MyidGridAll','y_pred']]
            print(f'hour {hour}: {len(r)} points')
            merged=wrf.merge(  r,on='MyidGridAll', how='left')

            merged['y_pred'] = np.where(merged['y_pred']/merged['y_pred']!=1, -9999, merged['y_pred'])
            merged['wrf_w'] = np.where(merged['wrf_w']/merged['wrf_w']!=1, -9999, merged['wrf_w'])

            
            merged['y_pred'] = np.where(merged['wrf_w']==9999, -9999, merged['y_pred'])

            
            pred1D=np.array(merged["y_pred"])
            wrf1D=np.array(merged["wrf_w"])
            
            

            if  interp != 0:
                #Interpolacion
                for zz in range(1,3):
                    pred1D=interpolar(pred1D,zz)


                #comparacionstation(df,pred1D,OUTPUT_FOLDER,f'before_i2_{inter_conv}',hour)


            pred2D=pred1D.reshape(480,396)
            if conv != 0:
                pred_val = pred1D[pred1D !=-9999]
                #print(pred_val)
                
                #Convolucion
                valor_mean = round(np.mean(pred_val),2)
                pred2D = np.where(pred2D == -9999,valor_mean, pred2D)
                #print('Primera opcion')
                #print(pred2D)
                tam = conv
                scharr =np.ones((tam, tam))/(tam**2)
                # scharr = np.array([  [1/9,1/9,1/9],
                #                     [1/9,1/9,1/9],
                #                     [1/9,1/9,1/9]])
                pred2D= signal.convolve2d(pred2D, scharr, boundary='wrap', mode='same')
                #print('Segunda opcion')
                #print(pred2D)
                for ii in range(480):
                    for jj in range(396):
                        if round(pred2D[ii][jj],6) == round(valor_mean,6):
                            pred2D[ii][jj] = -9999

                #pred2D[pred2D==valor_mean]= -9999
                #pred2D = np.where(pred2D < -1, -9999, pred2D)
                #print(pred2D)
            

                pred1D=pred2D.reshape(-1)
                #comparacionstation(df,pred1D,OUTPUT_FOLDER,f'before_conv_{inter_conv}',hour)

            #print(pred1D)
            merged["y_pred_inter"] = pred1D 
            merged["y_pred_inter"] = np.where(merged["y_pred_inter"]<0, -9999, merged["y_pred_inter"])
            merged["y_pred_inter"] = np.where(merged["y_pred_inter"]/merged["y_pred_inter"]!=1, -9999, merged["y_pred_inter"])
            
            merged['ratio'] = merged["y_pred_inter"]/merged["wrf_w"]
            merged['ratio'] = np.where(merged['ratio']<0, -9999, merged['ratio'])
            ratio=np.array(merged['ratio'])
            zz =ii-int(24*int(ii/24))
            if comp_station !=0 and file_station != "":
                print("_Comp_station_")
                df = date_station_pge(zz,date,WORKING_FOLDER,file_station)
                comparacionstation(df, merged,OUTPUT_FOLDER,fecha_ar,zz)

            wrf2D=wrf1D.reshape(480,396)
            ratio2D=ratio.reshape(480,396)
            listOfDF.append(pred2D)
            listOfWRF.append(wrf2D)
            listOfrt2D.append(ratio2D)
    
    pred3D=np.array(listOfDF)
    WRF3D=np.array(listOfWRF)
    rat3D=np.array(listOfrt2D)

    pred3D = np.where(pred3D == -9999, np.nan, pred3D)
    WRF3D = np.where(WRF3D == -9999, np.nan, WRF3D)
    rat3D = np.where(rat3D == -9999, np.nan, rat3D)

    with Dataset(file_in,'r') as ncin:
        with Dataset(file_out, "w", format="NETCDF4") as ncout:

            #print(ncin.variables["Times"].shape[0]) #33
            # define axis size
            #---------------------------
            ncout.createDimension("time", None)
            ncout.createDimension("xx", ncin.dimensions["xx"].size)
            ncout.createDimension("yy",  ncin.dimensions["yy"].size)
            
            #ncout.createDimension("DateStrLen",  ncin.dimensions["DateStrLen"].size)
            
            #create dimension variables
            #---------------------------
            '''
            vout_latitude = ncout.createVariable( "latitude", "float", ( "yy", "xx"),  )
            vout_latitude[:] = ncin.variables["latitude"][:,:]
            
            vout_latitude = ncout.createVariable( "longitude", "float", ( "yy", "xx"),  )
            vout_latitude[:] = ncin.variables['longitude'][:,:]
            '''
            vout_U10 = ncout.createVariable( 'U10', "float", ("time","yy", "xx"),  )
            vout_U10[:,:,:] = ncin.variables['U10'][int(hour_ini):int(hour_fin)+1,:,:].astype(float)

            vout_v10 = ncout.createVariable( 'V10', "float", ("time","yy", "xx"),  )
            vout_v10[:,:,:] = ncin.variables['V10'][int(hour_ini):int(hour_fin)+1,:,:].astype(float)

            vout_temp = ncout.createVariable( "T2", "float", ("time","yy", "xx"),  )
            vout_temp[:,:,:] = ncin.variables["T2"][int(hour_ini):int(hour_fin)+1,:,:].astype(float)

            vout_rh = ncout.createVariable( "rh", "float", ("time","yy", "xx"),  )
            vout_rh[:,:,:] = ncin.variables["rh"][int(hour_ini):int(hour_fin)+1,:,:].astype(float)
            
            #vout_y_pred = ncout.createVariable( "pred_w", "float", ("time","yy", "xx"),  )       
            #vout_y_pred[:,:,:] = pred3D

            #vout_WRF_W = ncout.createVariable( "wrf_w", "float", ("time","yy", "xx"),  )       
            #vout_WRF_W[:,:,:] = WRF3D
            
            vout_RATIO = ncout.createVariable( "ratio", "float", ("time","yy", "xx"), zlib=True, least_significant_digit=2,complevel=8 ) 
            vout_RATIO.units = 'pred/wrf_w'      
            vout_RATIO[:,:,:] = rat3D


def CreateNetcdf_f_dfm(INPUT_FOLDER, OUTPUT_FOLDER, BYPASS_FOLDER, hour, hour_ini, hour_fin,path,file,WORKING_FOLDER,tipo_wrf,file_station,comp_station,auto_mean,conv,interp,name_out):
    if int(hour) >=24:
        real_hour = hour
        hour = int(hour) -24
    else:
        real_hour = hour
    crearcarpeta(OUTPUT_FOLDER) 
    listOfDF=[]
    listOfWRF=[]
    listOfrt2D=[]
    wrfName=ntpath.basename(file)
    file_in =INPUT_FOLDER + wrfName 
    #file_out =OUTPUT_FOLDER+ name_out +"_"+wrfName[:-3]+"_rt_.nc" 
    
    
    date=datetime.datetime.strptime(wrfName[:-6], 'dfm_lfm_sample_%Y%m%d_')
    fecha_ar = date.strftime("%d%m%Y")   
    file_out =OUTPUT_FOLDER+ name_out +"_"+f"{fecha_ar}"+"_rt_.nc" 

    RESULT_FOLDER = BYPASS_FOLDER + f"/Result_{tipo_wrf}/" 
    with Dataset(file,'r') as ncin:
        for ii in range(hour_ini, hour_fin+1):
            wrf=pd.DataFrame()
            _u=ncin.variables["U10"][ii,:,:]*(1/10)*0.621371
            _u = np.reshape( _u, -1)
            _v=ncin.variables["V10"][ii,:,:]*(1/10)*0.621371
            _v = np.reshape( _v, -1)
            _lfm=ncin.variables["lfm"][ii,:,:]
            _lfm = np.reshape( _lfm, -1)
            wrf[f'wrf_u'] =_u.round(2)
            wrf[f'wrf_v'] =_v.round(2)
            null_columns=wrf.columns[wrf.isnull().any()]
            wrf[null_columns].isnull().sum()
            wrf = wrf.dropna(how='any')
            null_columns=wrf.columns[wrf.isnull().any()]
            wrf[null_columns].isnull().sum()  
            wrf[f'wrf_w']= round(np.sqrt(wrf[f'wrf_u'].pow(2) + wrf[f'wrf_v'].pow(2)),2)
            wrf[f'wrf_dir']= round((180.0/math.pi) * np.arctan2(wrf[f'wrf_u'], wrf[f'wrf_v']),2)
            wrf['MyidGridAll'] =wrf.index

            if ii>=24:
                date2 = date + datetime.timedelta(days=int(ii/24))
                wrf['hour']= ii-int(24*int(ii/24))
                hour_work = ii-int(24*int(ii/24))
                wrf['day']= date2.strftime("%d")
                wrf['month']= date2.strftime("%m")
                wrf['year']= date2.strftime("%Y")
            else:
                wrf['hour']= ii
                hour_work = ii
                wrf['day']= date.strftime("%d")
                wrf['month']= date.strftime("%m")
                wrf['year']= date.strftime("%Y")
            


            fecha_ar = date.strftime("%d%m%Y")
            results=pd.read_csv(RESULT_FOLDER+f'Predict_Result_{hour_work}_{fecha_ar}_.csv')#, index_col=[0])
        
            r=results[['MyidGridAll','y_pred', 'y_pred_ori']]
            print(f'hour {ii}: {len(r)} points')
            merged=wrf.merge(  r,on='MyidGridAll', how='left')
            #print(merged)
            merged['y_pred'] = np.where(merged['y_pred']/merged['y_pred']!=1, -9999, merged['y_pred'])
            merged['wrf_w'] = np.where(merged['wrf_w']/merged['wrf_w']!=1, -9999, merged['wrf_w'])
            
            merged['y_pred'] = np.where(merged['wrf_w']==9999, -9999, merged['y_pred'])
            
            merged['y_pred_ori'] = np.where(merged['y_pred_ori']/merged['y_pred_ori']!=1, -9999, merged['y_pred_ori'])
            
            pred1D=np.array(merged["y_pred"])
            wrf1D=np.array(merged["wrf_w"])
            predori=np.array(merged["y_pred_ori"])
            
            

            
            if interp != 0:
                #Interpolacion
                print("_Performing_Interpolation_")
                for zz in range(1,interp):
                    pred1D=interpolar(pred1D,zz)


            pred2D=pred1D.reshape(480,396)
            if conv != 0:
                print("_Performing_Convolution_")
                pred_val = pred1D[pred1D !=-9999]
                #print(pred_val)
                
                #Convolucion
                valor_mean = round(np.mean(pred_val),2)
                pred2D = np.where(pred2D == -9999,valor_mean, pred2D)
                #print('Primera opcion')
                #print(pred2D)
                tam = conv
                scharr =np.ones((tam, tam))/(tam**2)
                # scharr = np.array([  [1/9,1/9,1/9],
                #                     [1/9,1/9,1/9],
                #                     [1/9,1/9,1/9]])
                pred2D= signal.convolve2d(pred2D, scharr, boundary='wrap', mode='same')
                #print('Segunda opcion')
                #print(pred2D)
                for ii in range(480):
                    for jj in range(396):
                        if round(pred2D[ii][jj],6) == round(valor_mean,6):
                            pred2D[ii][jj] = -9999

                #pred2D[pred2D==valor_mean]= -9999
                #pred2D = np.where(pred2D < -1, -9999, pred2D)
                #print(pred2D)
            

                pred1D=pred2D.reshape(-1)
                #comparacionstation(df,pred1D,OUTPUT_FOLDER,f'before_conv_{inter_conv}',hour)
            if auto_mean !=0:
                print("_Performing_Refill_")
                
                df_sts = pd.read_csv(WORKING_FOLDER+"grid.csv")
                valores=np.array(df_sts["MyidGridAll"])
                pred_val = pred1D[pred1D !=-9999]
                valor_mean = round(np.mean(pred_val),2)
                for zz in range(len(valores)):
                    if pred1D[valores[zz]] ==-9999.0 and _lfm[valores[zz]] !=-99:
                        #print(pred1D[zz], wrf1D[zz])
                        pred1D[valores[zz]] = valor_mean
            
            #print(pred1D)
            merged['y_pred_ori'] = predori
            merged["y_pred_inter"] = pred1D 
            merged["y_pred_inter"] = np.where(merged["y_pred_inter"]<0, -9999, merged["y_pred_inter"])
            merged["y_pred_inter"] = np.where(merged["y_pred_inter"]/merged["y_pred_inter"]!=1, -9999, merged["y_pred_inter"])
            merged['ratio'] = merged["y_pred_inter"]/merged["wrf_w"]
            merged['ratio'] = np.where(merged['ratio']<0, -9999, merged['ratio'])
            ratio=np.array(merged['ratio'])
            merged['y_pred_ori'] = np.where(merged['y_pred_ori']<0, -9999, merged['y_pred_ori'])
            zz =ii-int(24*int(ii/24))
            if comp_station !=0  and file_station != "":
                print("_Comp_station_")
                df = date_station_pge(zz,date,WORKING_FOLDER,file_station)
                #print(merged)
                comparacionstation(df, merged,OUTPUT_FOLDER,fecha_ar,zz)

            wrf2D=wrf1D.reshape(480,396)
            ratio2D=ratio.reshape(480,396)
            listOfDF.append(pred2D)
            listOfWRF.append(wrf2D)
            listOfrt2D.append(ratio2D)
    
    pred3D=np.array(listOfDF)
    WRF3D=np.array(listOfWRF)
    rat3D=np.array(listOfrt2D)

    pred3D = np.where(pred3D == -9999, np.nan, pred3D)
    WRF3D = np.where(WRF3D == -9999, np.nan, WRF3D)
    rat3D = np.where(rat3D == -9999, np.nan, rat3D)

    with Dataset(file_in,'r') as ncin:
        with Dataset(file_out, "w", format="NETCDF4") as ncout:

            #print(ncin.variables["Times"].shape[0]) #33
            # define axis size
            #---------------------------
            ncout.createDimension("time", None)
            ncout.createDimension("xx", ncin.dimensions["xx"].size)
            ncout.createDimension("yy",  ncin.dimensions["yy"].size)
            
            #ncout.createDimension("DateStrLen",  ncin.dimensions["DateStrLen"].size)
            
            #create dimension variables
            #---------------------------
            
            vout_latitude = ncout.createVariable( "latitude", "float", ( "yy", "xx"),  )
            vout_latitude[:] = ncin.variables["latitude"][:,:]
            
            vout_latitude = ncout.createVariable( "longitude", "float", ( "yy", "xx"),  )
            vout_latitude[:] = ncin.variables['longitude'][:,:]
            
            vout_U10 = ncout.createVariable( 'U10', "int16", ("time","yy", "xx"),  )
            vout_U10[:,:,:] = ncin.variables['U10'][int(hour_ini):int(hour_fin)+1,:,:].astype(int)

            vout_v10 = ncout.createVariable( 'V10', "int16", ("time","yy", "xx"),  )
            vout_v10[:,:,:] = ncin.variables['V10'][int(hour_ini):int(hour_fin)+1,:,:].astype(int)

            vout_temp = ncout.createVariable( "air_temperature_2m", "int16", ("time","yy", "xx"),  )
            vout_temp[:,:,:] = ncin.variables["air_temperature_2m"][int(hour_ini):int(hour_fin)+1,:,:].astype(int)

            vout_rh = ncout.createVariable( "air_relative_humidity_2m", "int16", ("time","yy", "xx"),  )
            vout_rh[:,:,:] = ncin.variables["air_relative_humidity_2m"][int(hour_ini):int(hour_fin)+1,:,:].astype(int)

            vout_fpi = ncout.createVariable( "FPI", "int16", ( "time", "yy", "xx"),  )
            vout_fpi[:,:,:] = ncin.variables["FPI"][:,:,:][int(hour_ini):int(hour_fin)+1,:,:].astype(int)
            
            vout_fpi_ipw = ncout.createVariable( "FPI_IPW", "int16", ( "time", "yy", "xx"),  )
            vout_fpi_ipw[:,:,:] = ncin.variables["FPI_IPW"][:,:,:][int(hour_ini):int(hour_fin)+1,:,:].astype(int)

            vout_ipw = ncout.createVariable( "IPW", "int16", ( "time", "yy", "xx"),  )
            vout_ipw[:,:,:] = ncin.variables["IPW"][int(hour_ini):int(hour_fin)+1,:,:].astype(int)
            
            vout_lfm = ncout.createVariable( 'lfm', "int16", ("time","yy", "xx"),  )
            vout_lfm[:,:,:] = ncin.variables['lfm'][int(hour_ini):int(hour_fin)+1,:,:].astype(int)

            vout_m100h = ncout.createVariable( 'm100h', "int16", ("time","yy", "xx"),  )
            vout_m100h[:,:,:] = ncin.variables['m100h'][int(hour_ini):int(hour_fin)+1,:,:].astype(int)

            vout_m10h = ncout.createVariable( 'm10h', "int16", ("time","yy", "xx"),  )
            vout_m10h[:,:,:] = ncin.variables['m10h'][int(hour_ini):int(hour_fin)+1,:,:].astype(int)

            vout_m1h = ncout.createVariable( 'm1h', "int16", ("time","yy", "xx"),  )
            vout_m1h[:,:,:] = ncin.variables['m1h'][int(hour_ini):int(hour_fin)+1,:,:].astype(int)

            vout_th= ncout.createVariable( "tech_herb", "float", ( "yy", "xx"),  )
            vout_th[:,:] = ncin.variables["tech_herb"][:,:]
            
            vout_thw = ncout.createVariable( "tech_woody", "float", ( "yy", "xx"),  )
            vout_thw[:,:] = ncin.variables['tech_woody'][:,:]     
            
            #vout_y_pred = ncout.createVariable( "pred_w", "float", ("time","yy", "xx"),  )       
            #vout_y_pred[:,:,:] = pred3D

            #vout_WRF_W = ncout.createVariable( "wrf_w", "float", ("time","yy", "xx"),  )       
            #vout_WRF_W[:,:,:] = WRF3D
            
            vout_RATIO = ncout.createVariable( "ratio", "float", ("time","yy", "xx"), zlib=True, least_significant_digit=2,complevel=8 ) 
            vout_RATIO.units = 'pred/wrf_w'      
            vout_RATIO[:,:,:] = rat3D


def modelo_interp_rf(df,tipo_stacion,hour,numero_repeticiones,i,tipo_ver,TIPO,variables_cluster,path,inter_conv,name_file,time_v,tipo_wrf, BYPASS_FOLDER, URL_MODEL):
    if int(hour) >=24:
        real_hour = hour
        hour = int(hour) -24
    else:
        real_hour = hour
        
    name2=ntpath.basename(name_file)
    date=datetime.datetime.strptime(name2[:-6], time_v)
    fecha_ar = date.strftime("%d%m%Y")    
    print('hour', hour)

    VALIDACION_FOLDER = BYPASS_FOLDER + f"{tipo_wrf}/{fecha_ar}/"
    NETCDF_FOLDER = BYPASS_FOLDER + f"/Result_{tipo_wrf}/"
    
    crearcarpeta(VALIDACION_FOLDER) 
    crearcarpeta(NETCDF_FOLDER) 

    for w in range(numero_repeticiones):
        
        MODEL_FOLDER =  URL_MODEL + f"cm_model/{tipo_stacion}/hour{hour}/"
        
        direccionEsta = os.path.exists(MODEL_FOLDER +f"{TIPO}_{i}_{hour}_maxmin_esc.pkl")
        
        if direccionEsta == True:
            estandarizar = joblib.load(MODEL_FOLDER +f"{TIPO}_{i}_{hour}_maxmin_esc.pkl")

        for n_cluster in [9000,8000,7000,6000,5000,4000,3000,2000, 1500, 1000, 500, 300,200,100]:
            try:
                df = df.drop(columns=['label'])
            except:
                pass
            
            MODEL_FOLDER =  URL_MODEL + f"cm_model/{tipo_stacion}/hour{hour}/"
            direccionEsta = os.path.exists(MODEL_FOLDER +f"{w}_{TIPO}_{i}_{n_cluster}_{hour}_kmeans.pkl")
            #print(direccionEsta, MODEL_FOLDER +f"{w}_{TIPO}_{i}_{n_cluster}_{hour}_kmeans.pkl")
            if direccionEsta == True:                    
                kmeans = joblib.load(MODEL_FOLDER +f"{w}_{TIPO}_{i}_{n_cluster}_{hour}_kmeans.pkl")
                df = LIBS.S_kmeans_.kmeansPredict(df[variables_cluster], df, kmeans, estandarizar) 
                
                MODEL_FOLDER =  URL_MODEL + f"cm_model/{tipo_stacion}/hour{hour}/"
                direccionEsta = os.path.exists(MODEL_FOLDER + f"{w}_w_{i}_{n_cluster}_{hour}_feature.txt")
                if direccionEsta == True:
                    feature = open(MODEL_FOLDER + f"{w}_w_{i}_{n_cluster}_{hour}_feature.txt",'r')
                    for linea in feature:
                        linea2 = linea.split(";")
                        Tipo = int(linea2[1])
                        df2 = df.query(f"label =={int(linea2[0])}") 
                        #varaiblesgroup=linea2[3].replace('\n','')
                        variables2 = linea2[2].split(',')
                        #print(variables2)
                        df3 = df2[variables2]
                        
                        name = f"{w}_{int(linea2[0])}_w_{i}_{n_cluster}_{hour}_rf.pkl"
                        #print(MODEL_FOLDER +name)
                        rf = joblib.load(MODEL_FOLDER +name)
                        if len(df3)>0:
                            y_pred= rf.predict(df3) 
                            
                            if Tipo ==1:
                                y_pred = y_pred + y_pred*0.1


                            if Tipo == 3:
                                y_pred = y_pred + y_pred*0.1
                                y_pred = y_pred + y_pred*0.15

                            if Tipo == 5:
                                y_pred = y_pred + y_pred*0.1
                                y_pred = y_pred + y_pred*0.15
                                y_pred = y_pred + y_pred*0.30

                            
                            df2['y_pred_ori'] = y_pred
                            df2 = ajuste_correccion(df2,0,hour,y_pred)
                            #df2['y_pred'] = y_pred+ y_pred*0.20
                            
                            df2['iter'] = w
                            df2['n_cluster'] = n_cluster
                            df2['tipo_c'] = 0
                            
                            #variables_select.append(linea2[3])
                            colum_sel = ['MyidGridAll','iter', 'n_cluster', 'tipo_c', 'lat', 'lon','day', 'month', 'year','hour', 'wrf_w', 'wrf_dir','label','y_pred_ori','y_pred']
                            
                            dataset = df2[colum_sel]

                            df = df[~df['label'].isin([int(linea2[0])])]
                            #df.drop(df2.index, inplace=True)
                            dataset.to_csv(VALIDACION_FOLDER + f"{w}_0_{int(linea2[0])}_w_{i}_{n_cluster}_{hour}_{fecha_ar}_pred.csv", index = False)

                  

    contenido = os.listdir(VALIDACION_FOLDER)
    archivos = []
    for fichero in contenido:

        if os.path.isfile(os.path.join(VALIDACION_FOLDER, fichero)) and fichero.endswith(f'_{hour}_{fecha_ar}_pred.csv'):
            archivos.append(fichero)
    contador =0
    for use_archivo in archivos:
        #print(RESULT_FOLDER+ use_archivo)
        df = pd.read_csv(VALIDACION_FOLDER+ use_archivo)
        if contador ==0:
            dataset = df
            contador = contador + 1
        else:
           dataset= pd.concat([dataset,df], sort = False) 
    #print(dataset)
    dataset.to_csv(NETCDF_FOLDER + f'Predict_Result_{hour}_{fecha_ar}_.csv', index = False) 
    if False:
        VAL_RE_FOLDER = URL_MODEL + f"validacion/{tipo_stacion}/Result/"
        df = pd.read_csv(VAL_RE_FOLDER +f"data_ori_{hour}_.csv")
        #print(df)

        print("STATION features....")
        df_sts = pd.read_csv(NETCDF_FOLDER + f'Predict_Result_{hour}_.csv')
        df = df.merge(df_sts, on=[ 'lat', 'lon','day', 'month', 'year','hour'])
        #df['y_wprd'] = df['y_wprd']*1.2
        df.to_csv(VAL_RE_FOLDER + f'ver_netcdf_{hour}.csv') 
    #CreateNetcdf(NETCDF_FOLDER, real_hour, hour,path,inter_conv) 



def modificat_dfm(INPUT_FOLDER, OUTPUT_FOLDER, hour_ini, hour_fin,path,file,dias_mas):
    #print(hour_ini, hour_fin)
    crearcarpeta(OUTPUT_FOLDER) 
    listOfDF=[]
    listOfWRF=[]
    listOfrt2D=[]
    wrfName=ntpath.basename(file)
    file_in =INPUT_FOLDER + wrfName 
    #file_out =OUTPUT_FOLDER+wrfName[:-6]+f"tr_.nc" 
    
    
    date=datetime.datetime.strptime(wrfName[:-6], 'dfm_lfm_sample_%Y%m%d_')
    if True:
        date = date + datetime.timedelta(days=dias_mas)

    fecha_ar = date.strftime("%Y%m%d")   

    file_out =OUTPUT_FOLDER+"dfm_lfm_sample_"+f"{fecha_ar}" +f"_tr_.nc" 


    with Dataset(file_in,'r') as ncin:
        with Dataset(file_out, "w", format="NETCDF4") as ncout:
            #print(ncin.variables)
            #print(ncin.variables["Times"].shape[0]) #33
            # define axis size
            #---------------------------
            ncout.createDimension("time", None)
            ncout.createDimension("xx", ncin.dimensions["xx"].size)
            ncout.createDimension("yy",  ncin.dimensions["yy"].size)
            
            #ncout.createDimension("DateStrLen",  ncin.dimensions["DateStrLen"].size)
            
            #create dimension variables
            #---------------------------
            
            vout_latitude = ncout.createVariable( "latitude", "float", ( "yy", "xx"),  )
            vout_latitude[:] = ncin.variables["latitude"][:,:]
            
            vout_latitude = ncout.createVariable( "longitude", "float", ( "yy", "xx"),  )
            vout_latitude[:] = ncin.variables['longitude'][:,:]
            
            vout_U10 = ncout.createVariable( 'U10', "int16", ("time","yy", "xx"),  )
            vout_U10[:,:,:] = ncin.variables['U10'][int(hour_ini):int(hour_fin)+1,:,:].astype(int)

            vout_v10 = ncout.createVariable( 'V10', "int16", ("time","yy", "xx"),  )
            vout_v10[:,:,:] = ncin.variables['V10'][int(hour_ini):int(hour_fin)+1,:,:].astype(int)

            vout_temp = ncout.createVariable( "air_temperature_2m", "int16", ("time","yy", "xx"),  )
            vout_temp[:,:,:] = ncin.variables["air_temperature_2m"][int(hour_ini):int(hour_fin)+1,:,:].astype(int)

            vout_rh = ncout.createVariable( "air_relative_humidity_2m", "int16", ("time","yy", "xx"),  )
            vout_rh[:,:,:] = ncin.variables["air_relative_humidity_2m"][int(hour_ini):int(hour_fin)+1,:,:].astype(int)

            vout_fpi = ncout.createVariable( "FPI", "int16", ( "time", "yy", "xx"),  )
            vout_fpi[:,:,:] = ncin.variables["FPI"][:,:,:][int(hour_ini):int(hour_fin)+1,:,:].astype(int)
            
            vout_fpi_ipw = ncout.createVariable( "FPI_IPW", "int16", ( "time", "yy", "xx"),  )
            vout_fpi_ipw[:,:,:] = ncin.variables["FPI_IPW"][:,:,:][int(hour_ini):int(hour_fin)+1,:,:].astype(int)

            vout_ipw = ncout.createVariable( "IPW", "int16", ( "time", "yy", "xx"),  )
            vout_ipw[:,:,:] = ncin.variables["IPW"][int(hour_ini):int(hour_fin)+1,:,:].astype(int)
            
            vout_lfm = ncout.createVariable( 'lfm', "int16", ("time","yy", "xx"),  )
            vout_lfm[:,:,:] = ncin.variables['lfm'][int(hour_ini):int(hour_fin)+1,:,:].astype(int)

            vout_m100h = ncout.createVariable( 'm100h', "int16", ("time","yy", "xx"),  )
            vout_m100h[:,:,:] = ncin.variables['m100h'][int(hour_ini):int(hour_fin)+1,:,:].astype(int)

            vout_m10h = ncout.createVariable( 'm10h', "int16", ("time","yy", "xx"),  )
            vout_m10h[:,:,:] = ncin.variables['m10h'][int(hour_ini):int(hour_fin)+1,:,:].astype(int)

            vout_m1h = ncout.createVariable( 'm1h', "int16", ("time","yy", "xx"),  )
            vout_m1h[:,:,:] = ncin.variables['m1h'][int(hour_ini):int(hour_fin)+1,:,:].astype(int)

            vout_th= ncout.createVariable( "tech_herb", "float", ( "yy", "xx"),  )
            vout_th[:,:] = ncin.variables["tech_herb"][:,:]
            
            vout_thw = ncout.createVariable( "tech_woody", "float", ( "yy", "xx"),  )
            vout_thw[:,:] = ncin.variables['tech_woody'][:,:]     



def unir_archivos(OUTPUT_FOLDER,fecha_ar):
    VALIDACION_FOLDER = OUTPUT_FOLDER +"Data/"
    
    contenido = os.listdir(VALIDACION_FOLDER)
    archivos = []
    for fichero in contenido:

        if os.path.isfile(os.path.join(VALIDACION_FOLDER, fichero)) and fichero.endswith(f'{fecha_ar}_.csv'):
            archivos.append(fichero)
    contador =0
    for use_archivo in archivos:
        #print(RESULT_FOLDER+ use_archivo)
        df = pd.read_csv(VALIDACION_FOLDER+ use_archivo)
        if contador ==0:
            dataset = df
            contador = contador + 1
        else:
           dataset= pd.concat([dataset,df], sort = False) 
    #print(dataset)
    OUTPUT_FOLDER_G = OUTPUT_FOLDER + "imagen/"
    LIBS.S_Analis_.AplotScatter(dataset["sts_w"].values, dataset["wrf_w"].values, dataset['y_pred_inter'].values, OUTPUT_FOLDER_G , f"comp_station_{fecha_ar}.png")


def unir_all(OUTPUT_FOLDER):
    VALIDACION_FOLDER = OUTPUT_FOLDER +"Data/"
    
    contenido = os.listdir(VALIDACION_FOLDER)
    archivos = []
    for fichero in contenido:

        if os.path.isfile(os.path.join(VALIDACION_FOLDER, fichero)) and fichero.endswith(f'.csv'):
            archivos.append(fichero)
    contador =0
    for use_archivo in archivos:
        #print(RESULT_FOLDER+ use_archivo)
        df = pd.read_csv(VALIDACION_FOLDER+ use_archivo)
        if contador ==0:
            dataset = df
            contador = contador + 1
        else:
           dataset= pd.concat([dataset,df], sort = False,ignore_index=True) 
    #print(dataset)
    OUTPUT_FOLDER_G = OUTPUT_FOLDER + "imagen/"
    LIBS.S_Analis_.AplotScatter(dataset["sts_w"].values, dataset["wrf_w"].values, dataset['y_pred_inter'].values, OUTPUT_FOLDER_G , f"comp_station_all.png")
    LIBS.S_Analis_.Only_AplotScatter(dataset["sts_w"].values, dataset["wrf_w"].values, 'blue', OUTPUT_FOLDER_G , f"comp_wrf_all.png", 'Modulus of wind speed in wrf (mph)')
    LIBS.S_Analis_.Only_AplotScatter(dataset["sts_w"].values, dataset['y_pred_inter'].values, 'red', OUTPUT_FOLDER_G , f"comp_ypred_all.png", 'Modulus of wind speed in pred (mph)')

    y_pred = dataset['y_pred_inter']
    PREDICT = 'y_pred_inter'
    TARGET = "sts_w"
    COMPARE ="wrf_w"


    #Error result all test
    error_ini, error_train, dev_ini, dev_train, des_st_ini, des_st_train = LIBS.S_Analis_.CompErr(dataset[TARGET].values, dataset[COMPARE].values,y_pred)
    
    LIBS.S_Analis_.histMulti(error_ini, error_train, dev_ini, dev_train, des_st_ini, des_st_train, dataset, PREDICT, COMPARE, TARGET,  OUTPUT_FOLDER_G, f"hist_station_all.png" ) 
    LIBS.S_Analis_.histMulti_big_range(error_ini, error_train, dev_ini, dev_train, des_st_ini, des_st_train, dataset, PREDICT, COMPARE, TARGET,  OUTPUT_FOLDER_G, f"hist_station_all_multi.png" )
    LIBS.S_Analis_.articulo_graf(error_ini, error_train, dev_ini, dev_train, des_st_ini, des_st_train, dataset, PREDICT, COMPARE, TARGET,OUTPUT_FOLDER_G, f"art_graf.png") 

    for ii in [0,10,20,30,40]:
        y_pred = dataset['y_pred_ori'] + dataset['y_pred_ori'] *(ii/100)
        dataset[f'y_pred{ii}'] = dataset['y_pred_ori'] + dataset['y_pred_ori'] *(ii/100)
        PREDICT = f'y_pred{ii}'
        TARGET = "sts_w"
        COMPARE ="wrf_w"
        OUTPUT_FOLDER_G = OUTPUT_FOLDER + "imagen/" + f"All_{ii}/"
        crearcarpeta(OUTPUT_FOLDER_G)
        #Error result all test
        error_ini, error_train, dev_ini, dev_train, des_st_ini, des_st_train = LIBS.S_Analis_.CompErr(dataset[TARGET].values, dataset[COMPARE].values,y_pred)
        
        LIBS.S_Analis_.histMulti(error_ini, error_train, dev_ini, dev_train, des_st_ini, des_st_train, dataset, PREDICT, COMPARE, TARGET,  OUTPUT_FOLDER_G, f"hist_station_all_{ii}.png" ) 
        LIBS.S_Analis_.histMulti_big_range(error_ini, error_train, dev_ini, dev_train, des_st_ini, des_st_train, dataset, PREDICT, COMPARE, TARGET,  OUTPUT_FOLDER_G, f"hist_station_all_multi_{ii}.png" )
        LIBS.S_Analis_.articulo_graf(error_ini, error_train, dev_ini, dev_train, des_st_ini, des_st_train, dataset, PREDICT, COMPARE, TARGET,OUTPUT_FOLDER_G, f"art_graf_{ii}.png") 

    for jj in range(24):
        dataset2 = dataset.query(f"hour == {jj}")
        if len(dataset2) >0:
            for ii in [0,10,20,30,40]:
                y_pred = dataset2['y_pred_ori'] + dataset2['y_pred_ori'] *(ii/100)
                dataset[f'y_pred{ii}'] = dataset2['y_pred_ori'] + dataset2['y_pred_ori'] *(ii/100)
                PREDICT = f'y_pred{ii}'
                TARGET = "sts_w"
                COMPARE ="wrf_w"
                OUTPUT_FOLDER_G = OUTPUT_FOLDER + "imagen/" + f"hour_{jj}/"
                crearcarpeta(OUTPUT_FOLDER_G)
                #Error result all test
                error_ini, error_train, dev_ini, dev_train, des_st_ini, des_st_train = LIBS.S_Analis_.CompErr(dataset2[TARGET].values, dataset2[COMPARE].values,y_pred)
                
                LIBS.S_Analis_.histMulti(error_ini, error_train, dev_ini, dev_train, des_st_ini, des_st_train, dataset2, PREDICT, COMPARE, TARGET,  OUTPUT_FOLDER_G, f"hist_station_all_hour{jj}_{ii}.png" ) 
                LIBS.S_Analis_.histMulti_big_range(error_ini, error_train, dev_ini, dev_train, des_st_ini, des_st_train, dataset2, PREDICT, COMPARE, TARGET,  OUTPUT_FOLDER_G, f"hist_station_all_multi_hour{jj}_{ii}.png" )
                LIBS.S_Analis_.articulo_graf(error_ini, error_train, dev_ini, dev_train, des_st_ini, des_st_train, dataset2, PREDICT, COMPARE, TARGET,OUTPUT_FOLDER_G, f"art_graf_hour{jj}_{ii}.png") 