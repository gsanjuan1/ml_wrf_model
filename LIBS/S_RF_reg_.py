import math
import random
import pandas as pd
from sklearn.ensemble import RandomForestRegressor
from sklearn.feature_selection import RFE
import os
import joblib

#####
# RF_Regression(X_train, Y_train, X_test, X_Valid, min_sample_value, max_depth_value, name_file='feature_impo_mod_kmeans.txt')
# Definition: Regression RF.
# X_train: Sample show that they contain the elements to be used for the train.
# Y_train: Sample show that they contain the elements to be used for the train (Solution).
# X_test: Sample show that they contain the elements to be used for the test.
# X_Valid: Sample show that they contain the elements to be used for the Validation.
# min_sample_value: The minimum number of samples required to split an internal node.
# hour: time relative to the model
# folder: Folder where to save the results.
# max_depth_value: The maximum depth of the tree. If None, then nodes are expanded until all leaves are pure or until all leaves contain less than min_samples_split samples
# name_file: Name of the file to save the feature importances by default will be saved in the file feature_impo_mod_kmeans.txt.
# Return: Returns y_pred, y_train, y_vali correspond to the results after using the model for test, train and validation.
#####

def crearcarpeta(folder):

    os.makedirs(folder, exist_ok=True)

def RF_Regression(X_train, Y_train, X_test, X_Valid, min_sample_value, max_depth_value, folder, hour, name_file='feature_impo_mod_kmeans.txt'):

    MODEL = 'ALGORITM/basic/'
    crearcarpeta(MODEL)
    #Creation of the random forest regression model.
    featureList = list(X_train.columns)
    model = RandomForestRegressor(n_estimators=50, random_state=0, min_samples_split = min_sample_value, max_depth = max_depth_value, n_jobs=-1)
    model.fit(X_train, Y_train)
    feature_imp = pd.Series(model.feature_importances_, index=featureList).sort_values(ascending=False)
    
    joblib.dump(model, MODEL +f"basic_{hour}_rf.pkl")
    
    #Save the feauture importances
    f = open(folder + name_file,'a') 
    
    for i in range(len(featureList)): f.write(featureList[i] +  "," )
    f.write(f"\n" )
    
    for i in range(len(featureList)): f.write(str(feature_imp[featureList[i]])+ "," )
    f.write(f"\n" )
    f.close()
    
    # Apply the model
    y_train= model.predict(X_train)
    y_pred= model.predict(X_test)
    y_vali= model.predict(X_Valid)
    
    return y_pred, y_train, y_vali

def RF_Regression_comb(X_train, Y_train, min_sample_value, max_depth_value):


    #Creation of the random forest regression model.
    featureList = list(X_train.columns)
    model = RandomForestRegressor(n_estimators=50, random_state=1, min_samples_split = min_sample_value, max_depth = max_depth_value, n_jobs=-1)
    model.fit(X_train, Y_train)
    
    return model

def SelectFeature(X,Y, feature):  
    
    columns_names = X.columns.values
    selector = RFE(estimator = RandomForestRegressor(), n_features_to_select=feature, step=1)
    selector = selector.fit(X, Y)
    select = selector.support_
    select_var = []
    for i in range(len(select)):
        if select[i] == True:
            select_var.append(columns_names[i])    
    print(select_var)
    return select_var    

def SelectFeatureRanking(X,Y, feature):  
    
    columns_names = X.columns.values
    selector = RFE(estimator = RandomForestRegressor(), n_features_to_select=feature, step=1)
    selector = selector.fit(X, Y)
    select = selector.ranking_
    return select   