import math
import random
import pandas as pd

#####
# ElementSelect(df,regStation,queryS)
# Definition: Randomize the station and days for the train and the test.
# df: Sample show that they contain the elements to be used.
# regStation: Register containing the stations.('myid')
# querytrain: Select de query for train
# querytest: Select de query for test
# RandomDays: 'newday % 2 != 0'
# RandomStation: 'newid % 3 != 0'
# RandomDays and Station: 'newid % 3 != 0 & newday % 2 != 0'
# RandomFalse: 'myid % 2==0 & day % 2 != 0''
# Return: The elements that fulfill the query.
#####

def ElementSelect(df,regStation,querytrain, querytest):
    
    # Select the stations and Randomize.
    StationNum = df[regStation].unique()
  
    idStation = []
    idDay = []
    
    for i in StationNum: idStation.append(i)
    
    for i in range(1,32): idDay.append(i)
    
    random.shuffle(idStation)
    random.shuffle(idDay)
    
    df['newid'] = df[regStation]
    df['newday'] = df[regStation]
    
    # Randomize the days and Station
    count = 0
    for i in idStation:
        df.loc[df.MyidGridAll==i,'newid']=idStation[count]
        count = count + 1
    
    for i in range(1,32): df.loc[df.day==i,'newday']=idDay[i-1]
    
    # Query select
    df_train = df.query(querytrain)
    df_test = df.query(querytest)
    df.drop(['newid','newday'], axis=1)
    
    return  df_train, df_test

#####
# separateLastDays(df,monthX=12,nLastDays=3)
# Definition: Select the last 3 days for all seasons in a selected month.
# df: Sample show that they contain the elements to be used for the train.
# monthX: Month where the validation will be performed.
# nLastDays: Days used in validation.
# Return: Returns 2 bases, df is the base without the validation elements and finalDaysdf is the base with validation elements
#####

def separateLastDays(df,monthX=12,nLastDays=3):

    #Identify in df the last day of the selected month
    monthdf=df[df.month==monthX]
    daymax=monthdf.day.max()
    
    #Select the validation population.
    finalDaysdf=monthdf[(monthdf.day>(daymax-nLastDays)) & (monthdf.day<=daymax)]
    rows = finalDaysdf.index
    df.drop(rows, inplace=True)

    return df, finalDaysdf
