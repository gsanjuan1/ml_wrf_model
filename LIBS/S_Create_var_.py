import math
import random
import pandas as pd
import numpy as np
import ML_help_
import os
import LIBS.S_RF_reg_
import LIBS.S_Analis_
import LIBS.S_sel_
from sklearn.ensemble import RandomForestRegressor
from sklearn.feature_selection import RFE
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error, r2_score

def crearcarpeta(folder):

    os.makedirs(folder, exist_ok=True)
#####
# PercentilemothStation(df, regStation, folder)
# Definition: Creation of the variable to determine the percentiles of each of the seasons and month.
# df: Sample show that they contain the elements to be used.
# regStation: Register containing the stations.('MyidGridAll')
# regEvaluation: Register containing the values stations.('sts_w')
# folder: Folder where to save the results.
# Return: Returns 3 files with their percentiles.
#####

def PercentilemothStation(df, regStation, regEvaluation, folder):
   
    # Stations are selected.
    valores = df[regStation].unique()
    
    # Define de matrix
    tabla25 =np.zeros((len(valores)+1, 13))
    tabla50 =np.zeros((len(valores)+1, 13))
    tabla75 =np.zeros((len(valores)+1, 13))
    
    contar_valor=0
    #Percentiles are calculated. 
    for j in valores:
        contar_valor =contar_valor+1
        tabla25[contar_valor,0]=j
        tabla50[contar_valor,0]=j
        tabla75[contar_valor,0]=j
        
        for i in range(12):

            descripcion = df[regEvaluation][df[regStation]==j][df['month'] ==i+1].describe()

            tabla25[contar_valor,i+1]=descripcion[4]
            tabla50[contar_valor,i+1]=descripcion[5]
            tabla75[contar_valor,i+1]=descripcion[6]

    # Date save in the files.
    df2 =pd.DataFrame(tabla25, columns = ['MyidGridAll','25_1','25_2','25_3','25_4','25_5','25_6','25_7','25_8','25_9','25_10','25_11','25_12'])
    df2.to_csv(folder+'tabla25.csv')
    df2 =pd.DataFrame(tabla50, columns = ['MyidGridAll','50_1','50_2','50_3','50_4','50_5','50_6','50_7','50_8','50_9','50_10','50_11','50_12'])
    df2.to_csv(folder+'tabla50.csv')
    df2 =pd.DataFrame(tabla75, columns = ['MyidGridAll','75_1','75_2','75_3','75_4','75_5','75_6','75_7','75_8','75_9','75_10','75_11','75_12'])
    df2.to_csv(folder+'tabla75.csv')


#####
# SecondaryVariables(df)
# Definition: Generate the secondary variables needed to generate the random forest.
# df: Sample show that they contain the elements to be used.
# Return: Returns df with the new variables added.
#####
def SecondaryVariables(df):
    
    df['wrf_u'] = df['wrf_w']*np.sin((df['wrf_dir']-180)*math.pi/180)
    df['wrf_v'] = df['wrf_w']*np.cos((df['wrf_dir']-180)*math.pi/180)
    df['wrf_dem_ang'] = ML_help_.AngleDistance(df['wrf_dir'].values, df['dem_aspect'].values)

    df['sts_u'] = df['sts_w']*np.sin((df['sts_dir']-180)*math.pi/180)
    df['sts_v'] = df['sts_w']*np.cos((df['sts_dir']-180)*math.pi/180)

    #df = ML_help_.Add_Windnija_from_dir(df)
    df['ninja_u'] = df['ninja_w']*np.sin((df['ninja_dir']-180)*math.pi/180)
    df['ninja_v'] = df['ninja_w']*np.cos((df['ninja_dir']-180)*math.pi/180)
    df['ninja_w2'] = df['ninja_w']*df['wrf_w']/10
    df['ninja_dem_ang'] =ML_help_.AngleDistance(df['ninja_dir'].values, df['dem_aspect'].values)

    df['wind_event'] = np.where((df.sts_w < 15), 0, 1)

    null_columns=df.columns[df.isnull().any()]
    df[null_columns].isnull().sum()
    df = df.dropna(how='any')
    null_columns=df.columns[df.isnull().any()]
    df[null_columns].isnull().sum()
    
    return df

def SecondaryVariables2(df):
    
    #df['wrf_u'] = df['wrf_w']*np.sin((df['wrf_dir']-180)*math.pi/180)
    #df['wrf_v'] = df['wrf_w']*np.cos((df['wrf_dir']-180)*math.pi/180)
    df['wrf_dem_ang'] = ML_help_.AngleDistance(df['wrf_dir'].values, df['dem_aspect'].values)

    df['sts_u'] = df['sts_w']*np.sin((df['sts_dir'])*math.pi/180)
    df['sts_v'] = df['sts_w']*np.cos((df['sts_dir'])*math.pi/180)

    #df = ML_help_.Add_Windnija_from_dir(df)
    df['ninja_u'] = df['ninja_w']*np.sin((df['ninja_dir'])*math.pi/180)
    df['ninja_v'] = df['ninja_w']*np.cos((df['ninja_dir'])*math.pi/180)
    df['ninja_w2'] = df['ninja_w']*df['wrf_w']/10
    df['ninja_dem_ang'] =ML_help_.AngleDistance(df['ninja_dir'].values, df['dem_aspect'].values)

    df['wind_event'] = np.where((df.sts_w < 15), 0, 1)

    null_columns=df.columns[df.isnull().any()]
    df[null_columns].isnull().sum()
    df = df.dropna(how='any')
    null_columns=df.columns[df.isnull().any()]
    df[null_columns].isnull().sum()
    
    return df

def SecondaryVariablemodelo(df):
    
    #df['wrf_u'] = df['wrf_w']*np.sin((df['wrf_dir']-180)*math.pi/180)
    #df['wrf_v'] = df['wrf_w']*np.cos((df['wrf_dir']-180)*math.pi/180)
    df['wrf_dem_ang'] = ML_help_.AngleDistance(df['wrf_dir'].values, df['dem_aspect'].values)



    #df = ML_help_.Add_Windnija_from_dir(df)
    df['ninja_u'] = df['ninja_w']*np.sin((df['ninja_dir'])*math.pi/180)
    df['ninja_v'] = df['ninja_w']*np.cos((df['ninja_dir'])*math.pi/180)
    df['ninja_w2'] = df['ninja_w']*df['wrf_w']/10
    df['ninja_dem_ang'] =ML_help_.AngleDistance(df['ninja_dir'].values, df['dem_aspect'].values)


    null_columns=df.columns[df.isnull().any()]
    df[null_columns].isnull().sum()
    df = df.dropna(how='any')
    null_columns=df.columns[df.isnull().any()]
    df[null_columns].isnull().sum()
    
    return df


def studypopulation(df, i):

    if i ==1:
        df = df.query('hour<13')
 
    if i ==3:
        df = df.query('hour>=13')
    
    if i ==4:
        df = df.query('(hour<13) & (month==1 or month==2 or month==12)')
        
    if i ==5:
        df = df.query('(hour>=13) & (month==1 or month==2 or month==12)')

    if i ==6:
        df = df.query('(hour<13) & (month==3 or month==4 or month==5)')
        
    if i ==7:
        df = df.query('(hour>=13) & (month==3 or month==4 or month==5)')

    if i ==8:
        df = df.query('(hour<13) & (month==6 or month==7 or month==8)')
        
    if i ==9:
        df = df.query('(hour>=13) & (month==6 or month==7 or month==8)')

    if i ==10:
        df = df.query('(hour<13) & (month==9 or month==10 or month==11)')
        
    if i ==11:
        df = df.query('(hour>=13) & (month==9 or month==10 or month==11)')
    
    if i ==12:
        df = df 

    return df


def combinaciones(df, variables, feature, COMPARE, TARGET , TIPO, w,tipo_stacion):
    IMAGE_FOLDER = f"Metodologia/Image/{tipo_stacion}/"
    RESULT_FOLDER = f"Metodologia/Result/{tipo_stacion}/"

    #Create folder
    crearcarpeta(IMAGE_FOLDER)
    crearcarpeta(RESULT_FOLDER)

    # Split the data
    # --------------------
    estaciones = df['MyidGridAll'].unique()
    df_train2, df_test2 = LIBS.S_sel_.ElementSelect(df,'MyidGridAll','newday % 2 != 0','newday % 2 == 0')

    for entero_train in estaciones:
        df_train = df_train2[df_train2['MyidGridAll'].isin([entero_train])]
  

        if True:
            X = df_train[variables]
            Y = df_train[TARGET] 
            
            variables = LIBS.S_RF_reg_.SelectFeature(X,Y, feature)
            LIBS.S_Analis_.savefeature(entero_train, variables, feature, RESULT_FOLDER, f'feature_{TIPO}.txt')
        
        X_train = df_train[variables]
        Y_train = df_train[TARGET]  

        #Apply Random Forest
        model = LIBS.S_RF_reg_.RF_Regression_comb(X_train, Y_train,5,100)
        y_train= model.predict(X_train)

        #Error result trainning
        error_ini, error_train, dev_ini, dev_train, des_st_ini, des_st_train = LIBS.S_Analis_.CompErr(df_train[TARGET].values, df_train[COMPARE].values,y_train)

        #Hitograma result training
        LIBS.S_Analis_.ResultSimulation('TRAIN', entero_train, -1, X_train, error_ini, error_train, dev_ini, dev_train, w, RESULT_FOLDER, f'caracteristicas_eje_mod_comb_tipo{TIPO}.txt' )
        
        for entero_test in estaciones:
            df_test =  df_test2[df_test2['MyidGridAll'].isin([entero_test])]
            X_test = df_test[variables]
            Y_test = df_test[TARGET]
            y_pred= model.predict(X_test)

            #Error result test
            error_ini, error_train, dev_ini, dev_train, des_st_ini, des_st_train = LIBS.S_Analis_.CompErr(df_test[TARGET].values, df_test[COMPARE].values,y_pred)           
            
            
            LIBS.S_Analis_.ResultSimulation('TEST', entero_train, entero_test, X_test, error_ini, error_train, dev_ini, dev_train , w, RESULT_FOLDER, f'caracteristicas_eje_mod_comb_tipo{TIPO}.txt')
            
            if (len(error_ini[error_ini <-15])>=len(error_train[error_train <-15])) and (100-round(100*dev_train/dev_ini,0))>85:
                #Scatter result test
                LIBS.S_Analis_.AplotScatter(df_test[TARGET].values, df_test[COMPARE].values, y_pred, IMAGE_FOLDER, f"scat_test_{entero_train}_{w}_mod_comb_{entero_test}_tipo{TIPO}.png")
                #Hitograma result test
                LIBS.S_Analis_.histOnly(error_ini, error_train, dev_ini, dev_train, des_st_ini, des_st_train, IMAGE_FOLDER, f"hist_test_{w}_comb_{entero_train}_{entero_test}_tipo{TIPO}.png" ) 


def combinaciones_temporal(df, variables2, COMPARE, TARGET , TIPO, w, i,tipo_stacion, path):
    IMAGE_FOLDER = path + f"Metodologia/comb_temp/Image/{tipo_stacion}/"
    RESULT_FOLDER = path + f"Metodologia/comb_temp/Result/{tipo_stacion}/"

    #Create folder
    crearcarpeta(IMAGE_FOLDER)
    crearcarpeta(RESULT_FOLDER)

    estaciones = df['MyidGridAll'].unique()

    df_train2, df_test2 = LIBS.S_sel_.ElementSelect(df,'MyidGridAll','newday % 2 != 0','newday % 2 == 0')
    
    f = open(f'ejecucion{i}_{tipo_stacion}.txt' ,'a') 
    f.close()
    f = open(f'ejecucion{i}_{tipo_stacion}.txt' ,'r')
    estaciones_ejec = []
    for linea in f:
        estaciones_ejec.append(int(linea))

    
    for entero_train in estaciones:
        if entero_train not in estaciones_ejec:
            df_train = df_train2[df_train2['MyidGridAll'].isin([entero_train])]

            for j in range(5,26,5):
                print("feauture", j)
                if True:
                    X = df_train[variables2]
                    Y = df_train[TARGET] 
                    
                    variables = LIBS.S_RF_reg_.SelectFeature(X,Y, j)
                    LIBS.S_Analis_.savefeature(entero_train, variables, j, RESULT_FOLDER, f'feature_{TIPO}_{i}.txt')
                
                X_train = df_train[variables]
                Y_train = df_train[TARGET]  

                #Apply Random Forest
                model = LIBS.S_RF_reg_.RF_Regression_comb(X_train, Y_train,5,100)
                y_train= model.predict(X_train)

                #Error result trainning
                error_ini, error_train, dev_ini, dev_train, des_st_ini, des_st_train = LIBS.S_Analis_.CompErr(df_train[TARGET].values, df_train[COMPARE].values,y_train)

                #Hitograma result training
                LIBS.S_Analis_.ResultCombinacion(f'TRAIN', i, j, entero_train, -1, X_train, error_ini, error_train, dev_ini, dev_train, w, RESULT_FOLDER, f'caracteristicas_eje_mod_comb_tipo{TIPO}_{i}.txt' )
                
                for entero_test in estaciones:
                    df_test =  df_test2[df_test2['MyidGridAll'].isin([entero_test])]
                    X_test = df_test[variables]
                    Y_test = df_test[TARGET]
                    y_pred= model.predict(X_test)

                    #Error result test
                    error_ini, error_train, dev_ini, dev_train, des_st_ini, des_st_train = LIBS.S_Analis_.CompErr(df_test[TARGET].values, df_test[COMPARE].values,y_pred)           
                    
                    
                    LIBS.S_Analis_.ResultCombinacion(f'TEST', i, j, entero_train, entero_test, X_test, error_ini, error_train, dev_ini, dev_train , w, RESULT_FOLDER, f'caracteristicas_eje_mod_comb_tipo{TIPO}_{i}.txt')
                    
                    if (len(error_ini[error_ini <-15])>=len(error_train[error_train <-15])) and (100-round(100*dev_train/dev_ini,0))>88:
                        #Scatter result test
                        LIBS.S_Analis_.AplotScatter(df_test[TARGET].values, df_test[COMPARE].values, y_pred, IMAGE_FOLDER, f"scat_test_{entero_train}_{w}_mod_comb_{entero_test}_tipo{TIPO}_{i}_f_{j}.png")
                        #Hitograma result test
                        LIBS.S_Analis_.histOnly(error_ini, error_train, dev_ini, dev_train, des_st_ini, des_st_train, IMAGE_FOLDER, f"hist_test_{w}_comb_{entero_train}_{entero_test}_tipo{TIPO}_{i}_f_{j}.png" ) 
            
            f = open(f'ejecucion{i}_{tipo_stacion}.txt' ,'a') 
            f.write(str(entero_train))
            f.write('\n')
            f.close()


def combinaciones_temporal2(df, variables2, COMPARE, TARGET , TIPO, w, i,tipo_stacion):
    IMAGE_FOLDER = path + f"Metodologia/comb_temp2/Image/{tipo_stacion}/"
    RESULT_FOLDER = path + f"Metodologia/comb_temp2/Result/{tipo_stacion}/"
    
    #Create folder
    crearcarpeta(IMAGE_FOLDER)
    crearcarpeta(RESULT_FOLDER)
    
    PREDICT = f'y_{TIPO}prd'

    estaciones = df['MyidGridAll'].unique()

    df_train2, df_test2 = LIBS.S_sel_.ElementSelect(df,'MyidGridAll','newday % 2 != 0','newday % 2 == 0 ')
    
    f = open(f'ejecucion{i}_{tipo_stacion}.txt' ,'a') 
    f.close()
    f = open(f'ejecucion{i}_{tipo_stacion}.txt' ,'r')
    estaciones_ejec = []
    for linea in f:
        estaciones_ejec.append(int(linea))

    
    for entero_train in estaciones:
        if entero_train not in estaciones_ejec:
            df_train = df_train2[df_train2['MyidGridAll'].isin([entero_train])]


            if True and len(df_train)>0:
                X = df_train[variables2]
                Y = df_train[TARGET] 
                
                select_var = LIBS.S_RF_reg_.SelectFeatureRanking(X,Y, 5)
                
                for j in range(10,26,5):

                    variables = []
                    for tt in range(len(select_var)):
                        if select_var[tt]<=j:
                            variables.append(variables2[tt])
                    LIBS.S_Analis_.savefeature(entero_train, variables, j, RESULT_FOLDER, f'feature_{TIPO}_{i}.txt')
        
                    X_train = df_train[variables]
                    Y_train = df_train[TARGET]  

                    #Apply Random Forest
                    model = LIBS.S_RF_reg_.RF_Regression_comb(X_train, Y_train,5,100)
                    y_train= model.predict(X_train)

                    #Error result trainning
                    error_ini, error_train, dev_ini, dev_train, des_st_ini, des_st_train = LIBS.S_Analis_.CompErr(df_train[TARGET].values, df_train[COMPARE].values,y_train)

                    
                    #Hitograma result training
                    LIBS.S_Analis_.ResultCombinacion(f'TRAIN', i, j, entero_train, -1, X_train, error_ini, error_train, dev_ini, dev_train, w, RESULT_FOLDER, f'caracteristicas_eje_mod_comb_tipo{TIPO}_{i}.txt' )
                    contador =0
                    for entero_test in estaciones:
                        df_test =  df_test2[df_test2['MyidGridAll'].isin([entero_test])]
                        if len(df_test)>0:
                            X_test = df_test[variables]
                            Y_test = df_test[TARGET]
                            y_pred= model.predict(X_test)
                            df_test[PREDICT]= y_pred
                            #Error result test
                            error_ini, error_train, dev_ini, dev_train, des_st_ini, des_st_train = LIBS.S_Analis_.CompErr(df_test[TARGET].values, df_test[COMPARE].values,y_pred)           
                            X = df_test[TARGET].values.reshape(-1,1)

                            #print(entero_test)
                            print('men_square',mean_squared_error(X, y_pred))
                            LIBS.S_Analis_.ResultCombinacion2(f'TEST', i, j, entero_train, entero_test, X_test, error_ini, error_train, dev_ini, dev_train, mean_squared_error(X, y_pred), w, RESULT_FOLDER, f'caracteristicas_eje_mod_comb_tipo{TIPO}_{i}.txt')


                            #Scatter result test
                            LIBS.S_Analis_.AplotScatter(df_test[TARGET].values, df_test[COMPARE].values, y_pred, IMAGE_FOLDER, f"scat_test_{entero_train}_{w}_mod_comb_{entero_test}_tipo{TIPO}_{i}_f_{j}.png")
                            #Hitograma result test
                            LIBS.S_Analis_.histOnly(error_ini, error_train, dev_ini, dev_train, des_st_ini, des_st_train, IMAGE_FOLDER, f"hist_test_{w}_comb_{entero_train}_{entero_test}_tipo{TIPO}_{i}_f_{j}.png" ) 

                            colum_sel = ['MyidGridAll','day', 'month', 'hour', 'wrf_w', 'wrf_dir', 'wrf_v', 'wrf_u','sts_w',f'{PREDICT}']
                            dataset = df_test[colum_sel]

                    '''
                    try:
                        dataset.to_csv(RESULT_FOLDER + f'test_conbo2_{w}_{entero_train}_{j}.csv')
                    except:
                        print('ningun estacion optimo para feature', j)
                    '''

                f = open(f'ejecucion{i}_{tipo_stacion}.txt' ,'a') 
                f.write(str(entero_train))
                f.write('\n')
                f.close()


def analisispred(df, variables2, COMPARE, TARGET , TIPO, w, i,tipo_stacion):
    IMAGE_FOLDER = path + f"Metodologia/analisispred/Image/{tipo_stacion}/"
    RESULT_FOLDER = path + f"Metodologia/analisispred/Result/{tipo_stacion}/"
    
    #Create folder
    crearcarpeta(IMAGE_FOLDER)
    crearcarpeta(RESULT_FOLDER)
    
    PREDICT = f'y_{TIPO}prd'

    estaciones = df['MyidGridAll'].unique()
    df_val = df.query("day == 15")
    df_train2, df_test2 = LIBS.S_sel_.ElementSelect(df,'MyidGridAll','newday % 2 != 0 & day != 15','newday % 2 == 0 & day != 15')
    
    f = open(f'ejecucion{i}_{tipo_stacion}.txt' ,'a') 
    f.close()
    f = open(f'ejecucion{i}_{tipo_stacion}.txt' ,'r')
    estaciones_ejec = []
    for linea in f:
        estaciones_ejec.append(int(linea))

    
    for entero_train in estaciones:
        if entero_train not in estaciones_ejec:
            df_train = df_train2[df_train2['MyidGridAll'].isin([entero_train])]


            if True and len(df_train)>0:
                X = df_train[variables2]
                Y = df_train[TARGET] 
                
                select_var = LIBS.S_RF_reg_.SelectFeatureRanking(X,Y, 5)
                
                for j in range(10,26,5):

                    variables = []
                    for tt in range(len(select_var)):
                        if select_var[tt]<=j:
                            variables.append(variables2[tt])
                    LIBS.S_Analis_.savefeature(entero_train, variables, j, RESULT_FOLDER, f'feature_{TIPO}_{i}.txt')
        
                    X_train = df_train[variables]
                    Y_train = df_train[TARGET]  

                    #Apply Random Forest
                    model = LIBS.S_RF_reg_.RF_Regression_comb(X_train, Y_train,5,100)
                    y_train= model.predict(X_train)

                    #Error result trainning
                    error_ini, error_train, dev_ini, dev_train, des_st_ini, des_st_train = LIBS.S_Analis_.CompErr(df_train[TARGET].values, df_train[COMPARE].values,y_train)

                    
                    #Hitograma result training
                    LIBS.S_Analis_.ResultCombinacion(f'TRAIN', i, j, entero_train, -1, X_train, error_ini, error_train, dev_ini, dev_train, w, RESULT_FOLDER, f'caracteristicas_eje_mod_comb_tipo{TIPO}_{i}.txt' )
                    contador =0
                    
                    df_test =  df_test2[df_test2['MyidGridAll'].isin([entero_train])]
                    if len(df_test)>0:
                        X_test = df_test[variables]
                        Y_test = df_test[TARGET]
                        y_pred= model.predict(X_test)
                        df_test[PREDICT]= y_pred
                        #Error result test
                        error_ini, error_train, dev_ini, dev_train, des_st_ini, des_st_train = LIBS.S_Analis_.CompErr(df_test[TARGET].values, df_test[COMPARE].values,y_pred)           
                        X = df_test[TARGET].values.reshape(-1,1)

                        #print(entero_test)
                        print('men_square',mean_squared_error(X, y_pred))
                        LIBS.S_Analis_.ResultCombinacion2(f'TEST', i, j, entero_train, entero_train, X_test, error_ini, error_train, dev_ini, dev_train, mean_squared_error(X, y_pred), w, RESULT_FOLDER, f'caracteristicas_eje_test_mod_analisispred_tipo{TIPO}_{i}.txt')

                        #Scatter result test
                        LIBS.S_Analis_.AplotScatter(df_test[TARGET].values, df_test[COMPARE].values, y_pred, IMAGE_FOLDER, f"scat_test_{entero_train}_{w}_analisispred_tipo{TIPO}_{i}_f_{j}.png")
                        #Hitograma result test
                        LIBS.S_Analis_.histOnly(error_ini, error_train, dev_ini, dev_train, des_st_ini, des_st_train, IMAGE_FOLDER, f"hist_test_{w}_analisispred_{entero_train}_tipo{TIPO}_{i}_f_{j}.png" ) 

                        colum_sel = ['MyidGridAll','day', 'month', 'hour', 'wrf_w', 'wrf_dir', 'wrf_v', 'wrf_u','sts_w',f'{PREDICT}']
                        dataset = df_test[colum_sel]
                        contador =contador+1

                    
                        try:
                            dataset.to_csv(RESULT_FOLDER + f'test_{entero_train}_{j}.csv')
                        except:
                            print('ningun estacion optimo para feature', j)

                    df_val2 =  df_val[df_val['MyidGridAll'].isin([entero_train])]
                    if len(df_val2)>0:
                        X_test = df_val2[variables]
                        Y_test = df_val2[TARGET]
                        y_pred= model.predict(X_test)
                        df_val2[PREDICT]= y_pred
                        #Error result test
                        error_ini, error_train, dev_ini, dev_train, des_st_ini, des_st_train = LIBS.S_Analis_.CompErr(df_val2[TARGET].values, df_val2[COMPARE].values,y_pred)           
                        X = df_val2[TARGET].values.reshape(-1,1)

                        #print(entero_test)
                        print('men_square',mean_squared_error(X, y_pred))
                        LIBS.S_Analis_.ResultCombinacion2(f'VAL', i, j, entero_train, entero_train, X_test, error_ini, error_train, dev_ini, dev_train, mean_squared_error(X, y_pred), w, RESULT_FOLDER, f'caracteristicas_eje_mod_analisispred_val_tipo{TIPO}_{i}.txt')

                        #Scatter result test
                        LIBS.S_Analis_.AplotScatter(df_val2[TARGET].values, df_val2[COMPARE].values, y_pred, IMAGE_FOLDER, f"scat_val_{entero_train}_{w}_analisispred_tipo{TIPO}_{i}_f_{j}.png")
                        #Hitograma result test
                        LIBS.S_Analis_.histOnly(error_ini, error_train, dev_ini, dev_train, des_st_ini, des_st_train, IMAGE_FOLDER, f"hist_val_{w}_analisispred_{entero_train}_tipo{TIPO}_{i}_f_{j}.png" ) 

                        colum_sel = ['MyidGridAll','day', 'month', 'hour', 'wrf_w', 'wrf_dir', 'wrf_v', 'wrf_u','sts_w',f'{PREDICT}']
                        dataset = df_val2[colum_sel]
                        contador =contador+1

                    
                        try:
                            dataset.to_csv(RESULT_FOLDER + f'val_{entero_train}_{j}.csv')
                        except:
                            print('ningun estacion optimo para feature', j)
                    

                f = open(f'ejecucion{i}_{tipo_stacion}.txt' ,'a') 
                f.write(str(entero_train))
                f.write('\n')
                f.close()