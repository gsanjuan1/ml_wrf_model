import numpy as np
import pandas as pd
import math
from netCDF4 import Dataset
import glob
import ntpath
import datetime
import matplotlib.pyplot as plt
import LIBS.S_plot_
import LIBS.S_RF_reg_
import LIBS.S_Analis_
import LIBS.S_Create_var_
import LIBS.S_Modelo_Serv_
import seaborn as sns
import ML_help_
import sys
import warnings
import os
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import RandomForestRegressor
from sklearn.preprocessing import MinMaxScaler, OneHotEncoder
from sklearn import metrics  
import random
import gc
import json
from datetime import date,timedelta
import multiprocessing
from shutil import rmtree
from multiprocessing import Semaphore


warnings.filterwarnings("ignore")


file_config = sys.argv[1]

##### PARAMETER ####
# parametro_entrada: It defines that the data to parts of the division by hours does not have any other requirement
# numero_repeticiones: Number of steps currently has 5.
# ver: Number of elements to select.
######
parametro_entrada = 12 
numero_repeticiones = 2
ver = 0

myquery = 0 #Si es 0 ->PGE o 1 -> RAW

myquerytext =["SHORTNAME=='PGE'  & inCAL == 1",
              "SHORTNAME=='RAWS' & inCAL == 1"
              ]

nom_carp =['PGE',
          'RAWS']

if int(myquery) != -1:
    tipo_stacion =nom_carp[int(myquery)]
else:
   tipo_stacion = 'ALL' 



##### FOLDER ####

### WORKING_FOLDER: Folder the static data of the model is located.
#WORKING_FOLDER = f"datos/"

### INPUT_FOLDER: Folder with the NETCDF.
#INPUT_FOLDER = "C:/Users/Gemma/Desktop/python_WRF/pywind-main/CodiFin/netcdf/"

### OUT_FOLDER: Folder with output
#OUT_FOLDER ="netcdf/"

### MODEL_FOLDER: Folder with the model
#MODEL_FOLDER = f"C:/Users/Gemma/Desktop/python_WRF/pywind-main/CodiFin/Metodologia/Cluster_RF_3_0/"

### BYPASS_FOLDER: Folder with intermediate files (NOT CHANGE)
#BYPASS_FOLDER = "file/"

#name_out = 'aa'

ypredCorr = 0


def crearcarpeta(folder):

    os.makedirs(folder, exist_ok=True)

def prepare_netcdf(INPUT_FOLDER, file,tt,name,opt_hour):
    print("_read_netcdf_")
    name2=ntpath.basename(file)
    date=datetime.datetime.strptime(name2, 'Techno_wrfsfc_d03_%Y%m%d_0000.nc')
    with Dataset(INPUT_FOLDER+file,'r') as ncin:
        _temp=ncin.variables["T2"][tt,:,:]
        _temp = np.reshape(_temp, -1)
        _rh=ncin.variables["rh"][tt,:,:]*(1/10)
        _rh = np.reshape( _rh, -1)
        _u=ncin.variables["U10"][tt,:,:]*(1/10)*0.621371
        _u = np.reshape( _u, -1)
        _v=ncin.variables["V10"][tt,:,:]*(1/10)*0.621371
        _v = np.reshape( _v, -1)
        
        
        print("_read_static_parameters_")
        wrf = pd.DataFrame()
        wrf[f'wrf_temp{name}'] =_temp.round(2)
        wrf[f'wrf_rh{name}'] =_rh.round(2)
        wrf[f'wrf_u{name}'] =_u.round(2)
        wrf[f'wrf_v{name}'] =_v.round(2)
        
        null_columns=wrf.columns[wrf.isnull().any()]
        wrf[null_columns].isnull().sum()
        wrf = wrf.dropna(how='any')
        null_columns=wrf.columns[wrf.isnull().any()]
        wrf[null_columns].isnull().sum()  


        wrf[f'wrf_w{name}']= round(np.sqrt(wrf[f'wrf_u{name}'].pow(2) + wrf[f'wrf_v{name}'].pow(2)),2)
        wrf[f'wrf_dir{name}']= round((180.0/math.pi) * np.arctan2(wrf[f'wrf_u{name}'], wrf[f'wrf_v{name}']),2)
        wrf['MyidGridAll'] =wrf.index
    
        if opt_hour == True:
            if tt>=24:
                date2 = date + datetime.timedelta(days=int(tt/24))
                wrf['hour']= tt-int(24*int(tt/24))
                wrf['day']= date2.strftime("%d")
                wrf['month']= date2.strftime("%m")
                wrf['year']= date2.strftime("%Y")
            else:
                wrf['hour']= tt
                wrf['day']= date.strftime("%d")
                wrf['month']= date.strftime("%m")
                wrf['year']= date.strftime("%Y")

    return wrf

def prepare_netcdf_dfm(INPUT_FOLDER, file,tt,name,opt_hour):
    print("_read_netcdf_")
    name2=ntpath.basename(file)
    
    date=datetime.datetime.strptime(name2[:-6], 'dfm_lfm_sample_%Y%m%d_')
    with Dataset(INPUT_FOLDER+file,'r') as ncin:
        _temp=ncin.variables["air_temperature_2m"][tt,:,:]*(1/10) +273 #Origen Cent*(1/10)
        _temp = np.reshape(_temp, -1)
        _rh=ncin.variables["air_relative_humidity_2m"][tt,:,:]*(1/10) #Origen (1/10000)
        _rh = np.reshape( _rh, -1)
        _u=ncin.variables["U10"][tt,:,:]*(1/10)*0.621371 #km/h *(1/10)
        _u = np.reshape( _u, -1)
        _v=ncin.variables["V10"][tt,:,:]*(1/10)*0.621371 #km/h *(1/10)
        _v = np.reshape( _v, -1)
        
        
        print("_read_static_parameters_")
        wrf = pd.DataFrame()
        wrf[f'wrf_temp{name}'] =_temp.round(2)
        wrf[f'wrf_rh{name}'] =_rh.round(2)
        wrf[f'wrf_u{name}'] =_u.round(2)
        wrf[f'wrf_v{name}'] =_v.round(2)
        
        null_columns=wrf.columns[wrf.isnull().any()]
        wrf[null_columns].isnull().sum()
        wrf = wrf.dropna(how='any')
        null_columns=wrf.columns[wrf.isnull().any()]
        wrf[null_columns].isnull().sum()  


        wrf[f'wrf_w{name}']= round(np.sqrt(wrf[f'wrf_u{name}'].pow(2) + wrf[f'wrf_v{name}'].pow(2)),2)
        wrf[f'wrf_dir{name}']= round((180.0/math.pi) * np.arctan2(wrf[f'wrf_u{name}'], wrf[f'wrf_v{name}']),2)
        wrf['MyidGridAll'] =wrf.index
    
        if opt_hour == True:
            if tt>=24:
                date2 = date + datetime.timedelta(days=int(tt/24))
                wrf['hour']= tt-int(24*int(tt/24))
                wrf['day']= date2.strftime("%d")
                wrf['month']= date2.strftime("%m")
                wrf['year']= date2.strftime("%Y")
            else:
                wrf['hour']= tt
                wrf['day']= date.strftime("%d")
                wrf['month']= date.strftime("%m")
                wrf['year']= date.strftime("%Y")

    return wrf

def ninja(forecast_folder,df,name,opt_hour):
    print("NINJA features....")
    df_sts = pd.read_csv(forecast_folder +"ninja_data.csv")
    df = df.merge(df_sts, on=['MyidGridAll'])
    
    null_columns=df.columns[df.isnull().any()]
    df[null_columns].isnull().sum()
    df = df.dropna(how='any')
    null_columns=df.columns[df.isnull().any()]
    df[null_columns].isnull().sum()  

    for ang in [0,45,90,135,180,225,270]:

        
        df[f'diff_{ang}']= (df[f'wrf_dir{name}'] - df[f'ninja_dir_{ang}'] + 180) % 360 - 180

        df.loc[df[f'diff_{ang}']<-180, f'diff_{ang}'] = df.loc[df[f'diff_{ang}']<-180, f'diff_{ang}'] +360     

        df[f'diff_{ang}']=abs(df[f'diff_{ang}'])
        #print(df)
    df['min_sel'] = df[['diff_0', 'diff_45', 'diff_90','diff_135','diff_180','diff_225','diff_270']].idxmin(axis = 1) 
    #print(df)
    
    for ang in [0,45,90,135,180,225,270]:
        df3 = df.query(f"min_sel =='diff_{ang}'")
        
        df3[f'ninja_w{name}'] = round(df3[f'ninja_w_{ang}'],2)
        df3[f'ninja_dir{name}'] = round(df3[f'ninja_dir_{ang}'],2)
        #print(df3)

        if ang == 0:
            if opt_hour == True:
                df2 = df3[['MyidGridAll','year','month','day','hour',f'wrf_u{name}',f'wrf_v{name}',f'wrf_w{name}',f'wrf_dir{name}',f'wrf_temp{name}',f'wrf_rh{name}',f'ninja_w{name}', f'ninja_dir{name}']]
                dataset  = df2
            else:
                df2 = df3[['MyidGridAll',f'wrf_u{name}',f'wrf_v{name}',f'wrf_w{name}',f'wrf_dir{name}',f'wrf_temp{name}',f'wrf_rh{name}',f'ninja_w{name}', f'ninja_dir{name}']]
                dataset  = df2
            
        else:
            if opt_hour == True:
                df2 = df3[['MyidGridAll','year','month','day','hour',f'wrf_u{name}',f'wrf_v{name}',f'wrf_w{name}',f'wrf_dir{name}',f'wrf_temp{name}',f'wrf_rh{name}',f'ninja_w{name}', f'ninja_dir{name}']]
                dataset= pd.concat([dataset,df2], sort = False)
            else:
                df2 = df3[['MyidGridAll',f'wrf_u{name}',f'wrf_v{name}',f'wrf_w{name}',f'wrf_dir{name}',f'wrf_temp{name}',f'wrf_rh{name}',f'ninja_w{name}', f'ninja_dir{name}']]
                dataset= pd.concat([dataset,df2], sort = False)
            
    dataset = dataset.query(f"ninja_w{name}<1000")
    #print(dataset)
    return dataset

def staticparameter(WORKING_FOLDER,df):
    print("STATION features....")
    df_sts = pd.read_csv(WORKING_FOLDER+"grid.csv")
    df_sts = ML_help_.Landform_dummis(df_sts)
    df = df.merge(df_sts, on=['MyidGridAll'])
    
    null_columns=df.columns[df.isnull().any()]
    df[null_columns].isnull().sum()
    df = df.dropna(how='any')
    null_columns=df.columns[df.isnull().any()]
    df[null_columns].isnull().sum()      
    
    return df

def mean_values(df,INPUT_FOLDER,tt,file):

    print("_mean_value_")
    wrf = pd.DataFrame()
    name=ntpath.basename(file)
    date=datetime.datetime.strptime(name, 'Techno_wrfsfc_d03_%Y%m%d_0000.nc')
    with Dataset(INPUT_FOLDER+file,'r') as ncin:
        _temp=ncin.variables["T2"][2:26,:,:]
        _temp = np.reshape(_temp, -1)
        _rh=ncin.variables["rh"][2:26,:,:]*(1/10)
        _rh = np.reshape(_rh, -1)
        _u=ncin.variables["U10"][2:26,:,:]*(1/10)*0.621371
        _u = np.reshape(_u, -1)
        _v=ncin.variables["V10"][2:26,:,:]*(1/10)*0.621371
        _v = np.reshape(_v, -1)
        #time = ncin.variables['time'][:]
    count = 0
    
    with Dataset(INPUT_FOLDER+file,'r') as ncin:
        temp=ncin.variables["T2"][tt,:,:]
        temp = np.reshape(temp, -1)
        rh=ncin.variables["rh"][tt,:,:]*(1/10)
        rh = np.reshape(rh, -1)
        u=ncin.variables["U10"][tt,:,:]*(1/10)*0.621371
        u = np.reshape(u, -1)
        v=ncin.variables["V10"][tt,:,:]*(1/10)*0.621371
        v = np.reshape(v, -1)
        #time = ncin.variables['time'][:]
        '''
        for ii in range(50):
            #hace_una_hora = datetime.datetime.strptime('1970-01-01 00:00','%Y-%m-%d %H:%M') + timedelta(hours=int(time[ii]))
            temp=ncin.variables["T2"][ii,:,:] #Origen Cent*(1/10)
            #print('Techno', temp.mean(), 'hour',ii, hace_una_hora) 
            print('Techno', temp.mean(), 'hour',ii) 
        '''

    wrf.loc[count,'U10_mean']= round(_u.mean(),2)
    wrf.loc[count,'U10_std']= round(_u.std(),2)
    wrf.loc[count,'U10_mean_hour']= round(u.mean(),2)
    wrf.loc[count,'U10_std_hour']= round(u.std(),2) 

    wrf.loc[count,'V10_mean']= round(_v.mean(),2)
    wrf.loc[count,'V10_std']= round(_v.std(),2)
    wrf.loc[count,'V10_mean_hour']= round(v.mean(),2)
    wrf.loc[count,'V10_std_hour']= round(v.std(),2)

    wrf.loc[count,'rh_mean']= round(_rh.mean(),2)
    wrf.loc[count,'rh_std']= round(_rh.std(),2)
    wrf.loc[count,'rh_mean_hour']= round(rh.mean(),2)
    wrf.loc[count,'rh_std_hour']= round(rh.std(),2)

    wrf.loc[count,'temp_mean']= round(_temp.mean(),2)
    wrf.loc[count,'temp_std']= round(_temp.std(),2)
    wrf.loc[count,'temp_mean_hour']= round(temp.mean(),2)
    #print('Techno', temp.mean(), 'hour',tt)
    wrf.loc[count,'temp_std_hour']= round(temp.std(),2)

    if tt>=24:
        date2 = date + datetime.timedelta(days=int(tt/24))
        wrf.loc[count,'hour']= tt-int(24*int(tt/24))
        wrf.loc[count,'day']= date2.strftime("%d")
        wrf.loc[count,'month']= date2.strftime("%m")
        wrf.loc[count,'year']= date2.strftime("%Y")
    else:
        wrf.loc[count,'hour']= tt
        wrf.loc[count,'day']= date.strftime("%d")
        wrf.loc[count,'month']= date.strftime("%m")
        wrf.loc[count,'year']= date.strftime("%Y")
    count = count + 1
    df = df.merge(wrf, on=['year', 'month','day','hour'])
    del wrf,_temp,_rh,_u,_v,temp,rh,u,v
    gc.collect()
    return df  

def mean_values_dfm(df,INPUT_FOLDER,tt,file):

    print("_mean_value_")
    wrf = pd.DataFrame()
    name=ntpath.basename(file)
    date=datetime.datetime.strptime(name[:-6], 'dfm_lfm_sample_%Y%m%d_')
    with Dataset(INPUT_FOLDER+file,'r') as ncin:
        _temp=ncin.variables["air_temperature_2m"][2:26,:,:]*(1/10) +273
        _temp = np.reshape(_temp, -1)
        _rh=ncin.variables["air_relative_humidity_2m"][2:26,:,:]*(1/10)
        _rh = np.reshape(_rh, -1)
        _u=ncin.variables["U10"][2:26,:,:]*(1/10)*0.621371
        _u = np.reshape(_u, -1)
        _v=ncin.variables["V10"][2:26,:,:]*(1/10)*0.621371
        _v = np.reshape(_v, -1)
    count = 0
    
    with Dataset(INPUT_FOLDER+file,'r') as ncin:
        #print(ncin.variables)
        temp=ncin.variables["air_temperature_2m"][tt,:,:]*(1/10) +273 #Origen Cent*(1/10)
        temp = np.reshape(temp, -1)
        rh=ncin.variables["air_relative_humidity_2m"][tt,:,:]*(1/10) #Origen (1/10000)
        rh = np.reshape( rh, -1)
        u=ncin.variables["U10"][tt,:,:]*(1/10)*0.621371 #km/h *(1/10)
        u = np.reshape( u, -1)
        v=ncin.variables["V10"][tt,:,:]*(1/10)*0.621371 #km/h *(1/10)
        v = np.reshape( v, -1)
        
        #time = ncin.variables['time'][:]
        #for ii in range(50):
            #hace_una_hora = datetime.datetime.strptime('1970-01-01 00:00','%Y-%m-%d %H:%M') + timedelta(hours=int(time[ii]))
            #temp=ncin.variables["air_temperature_2m"][ii,:,:]*(1/10) +273 #Origen Cent*(1/10)
            #print('Techno', temp.mean(), 'hour',ii, hace_una_hora) 
            #print('Techno', temp.mean(), 'hour',ii) 
        
    
   
    
    wrf.loc[count,'U10_mean']= round(_u.mean(),2)
    wrf.loc[count,'U10_std']= round(_u.std(),2)
    wrf.loc[count,'U10_mean_hour']= round(u.mean(),2)
    wrf.loc[count,'U10_std_hour']= round(u.std(),2) 

    wrf.loc[count,'V10_mean']= round(_v.mean(),2)
    wrf.loc[count,'V10_std']= round(_v.std(),2)
    wrf.loc[count,'V10_mean_hour']= round(v.mean(),2)
    wrf.loc[count,'V10_std_hour']= round(v.std(),2)

    wrf.loc[count,'rh_mean']= round(_rh.mean(),2)
    wrf.loc[count,'rh_std']= round(_rh.std(),2)
    wrf.loc[count,'rh_mean_hour']= round(rh.mean(),2)
    wrf.loc[count,'rh_std_hour']= round(rh.std(),2)

    wrf.loc[count,'temp_mean']= round(_temp.mean(),2)
    #print('dfm', _temp.mean(), 'hour', tt)
    wrf.loc[count,'temp_std']= round(_temp.std(),2)
    wrf.loc[count,'temp_mean_hour']= round(temp.mean(),2)
    #print('dfm', temp.mean(), 'hour', tt)
    wrf.loc[count,'temp_std_hour']= round(temp.std(),2)

    if tt>=24:
        date2 = date + datetime.timedelta(days=int(tt/24))
        wrf.loc[count,'hour']= tt-int(24*int(tt/24))
        wrf.loc[count,'day']= date2.strftime("%d")
        wrf.loc[count,'month']= date2.strftime("%m")
        wrf.loc[count,'year']= date2.strftime("%Y")
    else:
        wrf.loc[count,'hour']= tt
        wrf.loc[count,'day']= date.strftime("%d")
        wrf.loc[count,'month']= date.strftime("%m")
        wrf.loc[count,'year']= date.strftime("%Y")
    count = count + 1
    df = df.merge(wrf, on=['year', 'month','day','hour'])
    del wrf,_temp,_rh,_u,_v,temp,rh,u,v
    gc.collect()
    return df  

def apply_model(INPUT_FOLDER, OUT_FOLDER, hour, WORKING_FOLDER,tipo_stacion,numero_repeticiones,parametro_entrada,ver,TIPO,variables,tipo_wrf, BYPASS_FOLDER, MODEL_FOLDER,file_station,comp_station,auto_mean,conv,interp,name_out,hour_ini_new, hour_fin_new):

    NETCDF_INPUT_FOLDER = OUT_FOLDER + f"dfm_{tipo_wrf}/"
    files = glob.glob(NETCDF_INPUT_FOLDER+'dfm_lfm_sample_*')
    if len(files)>0:
        for file in files:
            print('file: ', file, 'hour:', hour)
            df = prepare_netcdf_dfm("", file, int(hour),'',True)
            #forecast_folder = 'Data_techno/'
            df = ninja(WORKING_FOLDER,df,'',True)
            df_menosuno = prepare_netcdf_dfm("", file, int(hour)-1,'_menosuno',False)
            df_menosuno = ninja(WORKING_FOLDER,df_menosuno,'_menosuno',False)
            df_masuno = prepare_netcdf_dfm("", file, int(hour)+1,'_masuno',False)
            df_masuno = ninja(WORKING_FOLDER,df_masuno,'_masuno',False)
            df_menosdos = prepare_netcdf_dfm("", file, int(hour)-2,'_menosdos',False)
            df_menosdos = ninja(WORKING_FOLDER,df_menosdos,'_menosdos',False)
            df_masdos = prepare_netcdf_dfm("", file, int(hour)+2,'_masdos',False)
            df_masdos = ninja(WORKING_FOLDER,df_masdos,'_masdos',False)
            df = df.merge(df_menosuno, on=['MyidGridAll'])
            df = df.merge(df_masuno, on=['MyidGridAll'])
            df = df.merge(df_menosdos, on=['MyidGridAll'])
            df = df.merge(df_masdos, on=['MyidGridAll'])
            #Para verificar los datos
            #data =df[df['MyidGridAll'].isin([131248])]
            #data.to_csv("verificacion_datos.csv", index = False)
            df = staticparameter(WORKING_FOLDER,df)
            df = LIBS.S_Create_var_.SecondaryVariablemodelo(df)
            df =mean_values_dfm(df,"",int(hour),file)
            #print(df)

            time_v = 'dfm_lfm_sample_%Y%m%d_'
            LIBS.S_Modelo_Serv_.modelo_interp_rf(df,tipo_stacion,hour,int(numero_repeticiones),int(parametro_entrada),ver,TIPO,variables,'',0,file,time_v,tipo_wrf, BYPASS_FOLDER, MODEL_FOLDER)
            
            OUTPUT_FOLDER = OUT_FOLDER + f"Result_{tipo_wrf}/"
            hour_ini = hour_ini_new
            hour_fin = hour_fin_new
            
            count_hour = 0
            
            for ii in range(hour_ini_new, hour_fin_new+1):
                if ii>=24:
                    tt = ii-24
                else:
                    tt = ii 
                
                RESULT_FOLDER = BYPASS_FOLDER + f"/Result_{tipo_wrf}/"
                wrfName=ntpath.basename(file)
                date=datetime.datetime.strptime(wrfName[:-6], 'dfm_lfm_sample_%Y%m%d_')
                fecha_ar = date.strftime("%d%m%Y") 
                FILE = RESULT_FOLDER + f'Predict_Result_{tt}_{fecha_ar}_.csv'
                #print(FILE)
                if os.path.exists(FILE) == True:
                    count_hour = count_hour + 1
                #print(count_hour)
            if count_hour == (hour_fin_new - hour_ini_new) + 1:
                LIBS.S_Modelo_Serv_.CreateNetcdf_f_dfm(NETCDF_INPUT_FOLDER, OUTPUT_FOLDER,BYPASS_FOLDER, hour, hour_ini, hour_fin,'',file, WORKING_FOLDER,tipo_wrf,file_station,comp_station,auto_mean,conv,interp,name_out)
            
                if comp_station != 0:
                    for tipo_wrf in ['00','06','12','18']:  
                        #print('Entro')
                        OUTPUT_FOLDER = OUT_FOLDER + f"/Result_{tipo_wrf}/"
                        LIBS.S_Modelo_Serv_.unir_all(OUTPUT_FOLDER)
                


def Launcher( tipo):
    with open(file_config) as file:
        data = json.load(file)
    
    DATA_FOLDER = data["FOLDER_DATA"] 

    WORKING_FOLDER  = DATA_FOLDER + "data/"
    MODEL_FOLDER =  DATA_FOLDER
    BYPASS_FOLDER = DATA_FOLDER + "file/"
    
    INPUT_FOLDER = data["FOLDER_WORKING"]
    INPUT_NETCDF = data["NETCDF_INPUT"]
    OUT_FOLDER = data["FOLDER_WORKING"] 
    name_out = data["NETCDF_OUTPUT"]
    file_station = data["FILE_STATION"]
    hour_ini_new = int(data["HOUR_START"]) # Hour initial
    hour_fin_new = int(data["HOUR_END"]) # Hour finale
    interp = int(data["INTEPOLATION"])  #interpolate
    conv = int(data["CONVOLUTION"])  #convolucion
    auto_mean = int(data["AUTOMEAN"]) # ReFill with the mean of the map results.
    if file_station != "":
        comp_station = 1 # Compare with the station
    else:
        comp_station = 0 # Compare with the station
    try:
        rmtree(BYPASS_FOLDER) 
    except:
        pass
 
    
    COMPARE = f'wrf_{tipo}'
    TARGET = f'sts_{tipo}'
    PREDICT = f'pred_{tipo}'
    TIPO = tipo
    variables_geo =['lat','lon','metersToCoast']
    variables_ori =['insideArea','hexagon']
    variables_wrf =['wrf_v', 'wrf_u','wrf_w','wrf_temp', 'wrf_rh']
    variables_pred =['wrf_u_masuno','wrf_v_masuno','wrf_w_masuno','wrf_dir_masuno','wrf_temp_masuno','wrf_rh_masuno','ninja_w_masuno','ninja_dir_masuno','wrf_u_menosuno','wrf_v_menosuno','wrf_w_menosuno','wrf_dir_menosuno','wrf_temp_menosuno','wrf_rh_menosuno','ninja_w_menosuno','ninja_dir_menosuno']
    variables_demo = ['land_0.0','land_1.0','land_2.0','land_3.0','land_4.0','land_5.0','land_6.0','landformX_mean','cbd','cbd_mean','ch','chmean','cc','ccmean','cbh','cbhmean','fuel','fuelmean','dem','demmean','slope','dem_aspect']
    variables_time =['month', 'hour']
    variables_ninja =['ninja_w', 'ninja_dir','ninja_u','ninja_v','ninja_w2','ninja_dem_ang']
    variables_solar =['solar_1','solar_1_mean','solar_2','solar_2_mean','solar_3','solar_3_mean','solar_4','solar_4_mean','solar_5','solar_5_mean','solar_6','solar_6_mean','solar_7','solar_7_mean','solar_8','solar_8_mean','solar_9','solar_9_mean','solar_10','solar_10_mean','solar_11','solar_11_mean','solar_12','solar_12_mean']
    #variables_pred2 = ['wrf_u_masdos','wrf_v_masdos','wrf_w_masdos','wrf_dir_masdos','wrf_temp_masdos','wrf_rh_masdos','ninja_w_masdos','ninja_dir_masdos', 'wrf_u_menosdos','wrf_v_menosdos', 'wrf_w_menosdos', 'wrf_dir_menosdos','wrf_temp_menosdos','wrf_rh_menosdos', 'ninja_w_menosdos','ninja_dir_menosdos']


    variables = variables_geo + variables_wrf + variables_demo + variables_time + variables_solar + variables_pred + variables_ninja
    
    tipo_wrf = ''
    #Genero el archivo
    files = glob.glob(INPUT_FOLDER + INPUT_NETCDF)
    if len(files)>0:
        for file in files:        
            print(file)
            #print('archivo', file[-6:])
            if file[-6:] == '06z.nc':
                hour_ini =11
                hour_fin =40
                dias_mas = 1
                tipo_wrf ='06'
            if file[-6:] == '00z.nc':
                hour_ini =17
                hour_fin =47
                dias_mas = 0
                tipo_wrf ='00'
            if file[-6:] == '12z.nc':
                hour_ini =5
                hour_fin =35
                dias_mas = 0
                tipo_wrf ='12'
            if file[-6:] == '18z.nc':
                hour_ini =22
                hour_fin =52
                dias_mas = 2
                tipo_wrf ='18'
            
            OUTPUT_FOLDER = OUT_FOLDER + f"dfm_{tipo_wrf}/"
            
            try:
                rmtree(OUTPUT_FOLDER) 
            except:
                pass
            
            LIBS.S_Modelo_Serv_.modificat_dfm(INPUT_FOLDER, OUTPUT_FOLDER,hour_ini, hour_fin,'',file,dias_mas)
    if tipo_wrf != '':
        jobs = []
        for hour in range(hour_ini_new,hour_fin_new+1):
            p = multiprocessing.Process(target=apply_model, args=(INPUT_FOLDER, OUT_FOLDER, hour, WORKING_FOLDER,tipo_stacion,int(numero_repeticiones),int(parametro_entrada),ver,TIPO,variables,tipo_wrf, BYPASS_FOLDER, MODEL_FOLDER,file_station,comp_station,auto_mean,conv,interp,name_out,hour_ini_new, hour_fin_new,))

            jobs.append(p)
            p.start()
        p.join()
    else:
        print("NOT NETCDF")



def main():
    #simplificar_datos()
    
    for z in ['w']:
        Launcher(z)
        s = 1
    
    

if __name__ == "__main__":
   
    main()