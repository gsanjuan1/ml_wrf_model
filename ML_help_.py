import glob
import ntpath
from netCDF4 import Dataset
import datetime
import pandas as pd
import math
import numpy as np

WORKING_FOLDER = "C:/Users/Sanjuan-Z97/Desktop/windninja_c/python_WRF/"


def AngleDistance( a,  b):
    diff = (b - a + 180) % 360 - 180;
    diff[diff < -180]=diff[diff < -180] +360
    return abs(diff)  #en absoluto pq no nos importa la direccion del erro

def Add_Windnija_from_dir(df):
    '''
     chose the appropiate wind speed based on the wrf direction
    '''
    #no need to use two logical operations at each stpet beacuse np.select returns the first true value.
    conditions  = [ 
        (df['wrf_dir'] <22.5) | (df['wrf_dir'] >=360-22.5),
        df['wrf_dir'] <22.5+45, 
        df['wrf_dir'] <22.2+2*45,
        df['wrf_dir'] <22.5+3*45,
        df['wrf_dir'] <22.5+4*45,
        df['wrf_dir'] <22.5+5*45,
        df['wrf_dir'] <22.5+6*45,
        df['wrf_dir'] <22.5+7*45,
        ]
    
    choices_u=[]
    choices_v=[]
    for kk in range(8):
        choices_u.append(df[f'ninja_{kk}_w'])
        choices_v.append(df[f'ninja_{kk}_ang'])
  
    df["ninja_w"] = np.select(conditions, choices_u, default=np.nan)
    df["ninja_dir"] = np.select(conditions, choices_v, default=np.nan)
    return df

def Landform_dummis(df):
    conditions  = [ 
        df['landformX'] <14 ,
        df['landformX'] ==14, 
        df['landformX'] ==15,
        df['landformX'] <31,
        df['landformX'] <35,
        df['landformX'] ==41,
        df['landformX'] ==42   ]  
    
    choices=[0,1,2,3,4,5,6]
  
    df["landformX"] = np.select(conditions, choices, default=np.nan)
    df = pd.concat([df,pd.get_dummies(df['landformX'], prefix='land')],axis=1)
    df.drop(['landformX'],axis=1, inplace=True)
    return df



def add_u_v_to_windninja():
    df = pd.read_csv(WORKING_FOLDER+"STS_features.csv", nrows=100000)
    for mm in range(8):
        dirFrom=mm
        for kk in df.index:
            ang=df.loc[kk, f'ninja_{dirFrom}_ang']+180
            mod=df.loc[kk, f'ninja_{dirFrom}_w']
            df.loc[kk, f'ninja_{dirFrom}_u']=mod*math.sin(ang*math.pi /180)
            df.loc[kk, f'ninja_{dirFrom}_v']=mod*math.cos(ang*math.pi /180)
     
    df.to_csv(WORKING_FOLDER + "STS_features_ninja_hor.csv")

    
def filter_SD(df):
    #df= df.query('LAT > 33.6 & LON > -118') #santa catalina
    #df= df.query('LAT > 34.06  & LON > -119.25') #santa cruz
    #df= df.query('LON < -116')
    #df= df.query('LAT < 33.5') # remove all but san diego 
    
    df= df.query('LAT < 33.5  & LON > -117.66 & LON<-116')
    return df


def help_add_NAD_index():
    from osgeo import gdal
    LCP_PATH=r"D:\0WORK\stations\windninja\SD_dem_nad83_point_11-11-2020_0100_200m_vel.asc"
    CSV_FOLDER = "D:/0WORK/stations/SD/"
    data = gdal.Open(LCP_PATH,gdal.GA_ReadOnly)
    geo=data.GetGeoTransform()
    xmin=geo[0]
    ymax=geo[3]
    nrow=data.RasterYSize
    ncol=data.RasterXSize
    cellsize=geo[1]
    
    df_sts = pd.read_csv(CSV_FOLDER+"STATION_features.csv")
   
    for kk in df_sts.index:
        xx=df_sts.loc[kk, f'NAD_X']
        yy=df_sts.loc[kk, f'NAD_Y']
        ii=(int)((ymax-yy)/cellsize)
        jj =(int)((xx-xmin)/cellsize)
        if ii<0 or ii>nrow-1 or jj<0 or jj>ncol-1:
            ii=-1
            jj=-1
        df_sts.loc[kk, f'NAD_ii']=ii
        df_sts.loc[kk, f'NAD_jj']=jj
        print(ii,jj)
    df_sts.to_csv(CSV_FOLDER+"STATION_features_nadidx.csv",index=False)


def Create_NetCDF_projectionRaster(latmin,latmax,lonmin,lonmax,hx):
    from pyproj import Transformer
    transformer = Transformer.from_crs("epsg:4326", "epsg:3310")
    t_inv = Transformer.from_crs("epsg:3310", "epsg:4326")
    xmin,ymax=transformer.transform(latmax, lonmin)
    xmax,ymin =transformer.transform(latmin, lonmax)
  
    
    nrow=(ymax-ymin)/hx
    ncol=(xmax-ymin)/hx
    
    ras=np.Zeros(nrow,ncol)
    for ii in range(nrow):
        for jj in range(ncol):
            xx=xmin+jj*hx
            yy=ymax - ii*hx
            lat,lon=t_inv.transform(xx, yy)
    
    s=1


def main():
    
    s=1













if __name__ == "__main__":
    main()